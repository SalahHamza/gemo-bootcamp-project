/// <reference types="cypress" />

const image1Path = "./../assets/image-1.jpeg";
const canvasStageSelector = "#canvas-stage";

describe("Canvas test", () => {
  it("Visits canvas page", () => {
    cy.visit("http://localhost:3000");
  });

  it("Handles images upload", () => {
    const imageTabSelector = "[data-testid='image-tab']";

    // we open the image tabs
    cy.get("#image-tab-btn").click();

    // if there are no images, no canvas should be displayed
    cy.get(canvasStageSelector)
      .get("canvas")
      .should("not.exist");
    // and there should be no image tabs
    cy.get(imageTabSelector).should("not.exist");

    cy.addImage({
      imagePath: image1Path,
      input: "#add-image-input"
    });

    // after adding an element both canvas and an image
    // tab should exist
    cy.get(canvasStageSelector)
      .get("canvas")
      .should("exist")
      .should("be.visible");
    cy.get(imageTabSelector).should("exist");
  });

  it("Draws rect on canvas", () => {
    // we first test the rect drawing toolbar toggle
    const rectToolItemSelector = "[role='toolbar'] [aria-label='rectangle']";

    cy.get(rectToolItemSelector).should("have.attr", "aria-selected", "false");

    cy.get(rectToolItemSelector).click();

    cy.get(rectToolItemSelector).should("have.attr", "aria-selected", "true");

    cy.get(canvasStageSelector).then(canvasStage => {
      const shapeItemSelector = "[data-testid='shapes-panel-item']";

      cy.get(shapeItemSelector).should("not.exist");

      const boundingRect = canvasStage.get(0).getBoundingClientRect();
      drawRect({
        x: boundingRect.width / 2,
        y: boundingRect.height / 2,
        height: 100,
        width: 120
      });
      cy.get(shapeItemSelector).should("exist");

      cy.get(shapeItemSelector)
        .get("[aria-label='delete']")
        .click();

      cy.get(shapeItemSelector).should("not.exist");
    });
  });

  it("Draws circle on canvas", () => {
    // we first test the rect drawing toolbar toggle
    const circleToolItemSelector = "[role='toolbar'] [aria-label='circle']";

    cy.get(circleToolItemSelector).should("have.attr", "aria-selected", "false");

    cy.get(circleToolItemSelector).click();

    cy.get(circleToolItemSelector).should("have.attr", "aria-selected", "true");

    cy.get(canvasStageSelector).then(canvasStage => {
      const shapeItemSelector = "[data-testid='shapes-panel-item']";

      cy.get(shapeItemSelector).should("not.exist");

      const boundingRect = canvasStage.get(0).getBoundingClientRect();
      drawCircle({
        x: boundingRect.width / 2,
        y: boundingRect.height / 2,
        radius: 60
      });
      cy.get(shapeItemSelector).should("exist");
    });
  });

  function drawRect(rect) {
    const startPosition = {
      clientX: rect.x,
      clientY: rect.y,
      // here we are saying we want the main button
      // on the mouse to be clicked (the left button)
      button: 0
    };
    const endPosition = {
      clientX: rect.x + rect.width,
      clientY: rect.y + rect.height,
      button: 0
    };
    cy.get(canvasStageSelector)
      .get("canvas")
      .last()
      .trigger("mousedown", startPosition)
      .trigger("mousemove", endPosition)
      .trigger("mouseup");
  }
  function drawCircle(circle) {
    const startPosition = {
      clientX: circle.x,
      clientY: circle.y,
      // here we are saying we want the main button
      // on the mouse to be clicked (the left button)
      button: 0
    };
    const endPosition = {
      clientX: circle.x + circle.radius,
      button: 0
    };
    cy.get(canvasStageSelector)
      .get("canvas")
      .last()
      .trigger("mousedown", startPosition)
      .trigger("mousemove", endPosition)
      .trigger("mouseup");
  }
});
