module.exports = {
  extends: ["react-app", "plugin:prettier/recommended", "plugin:cypress/recommended"],
  plugins: ["prettier"],
  rules: {
    "react/prop-types": 0
  }
};
