import { makeStyles } from "@material-ui/styles";
import { Theme } from "@material-ui/core";

export default makeStyles((theme: Theme) => ({
  root: {
    height: "calc(100vh - 56px)",
    [theme.breakpoints.up("sm")]: {
      height: "calc(100vh - 64px)"
    },
    display: "flex"
  },
  main: {
    overflow: "hidden",
    borderRadius: 0,
    flexGrow: 1,
    flexShrink: 1,
    background: theme.palette.grey["700"],
    position: "relative"
  },
  aside: {
    width: 370,
    borderRadius: 0,
    flexShrink: 0,
    background: theme.palette.background.paper,
    borderLeft: `1px solid ${theme.palette.divider}`
  }
}));
