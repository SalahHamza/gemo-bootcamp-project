import React from "react";
import useStyles from "./styles";

/**
 * It's not a good idea to separate composable
 * components, so we keep them here.
 */

const Main: React.FC = ({ children }) => {
  const classes = useStyles();
  return <div className={classes.main}>{children}</div>;
};

const Side: React.FC = ({ children }) => {
  const classes = useStyles();
  return <div className={classes.aside}>{children}</div>;
};

type LayoutPanelComposition = {
  Main: React.FC;
  Side: React.FC;
};

const LayoutPanel: React.FC & LayoutPanelComposition = ({ children }) => {
  const classes = useStyles();

  return <div className={classes.root}>{children}</div>;
};

LayoutPanel.Main = Main;
LayoutPanel.Side = Side;

export default LayoutPanel;
