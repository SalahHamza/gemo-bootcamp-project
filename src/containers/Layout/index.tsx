import React from "react";
import useStyles from "./styles";
import Header from "components/Header";

const Layout: React.FC = ({ children }) => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Header />
      <main className={classes.main}>{children}</main>
    </div>
  );
};

export default Layout;
