import { makeStyles } from "@material-ui/styles";
import { Theme } from "@material-ui/core";

export default makeStyles(({ palette }: Theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    overflow: "hidden",
    height: "100vh",
    background: palette.background.default
  },
  main: {
    flexGrow: 1
  }
}));
