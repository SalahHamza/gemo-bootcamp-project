import React from "react";
import CanvasRoot from "components/Canvas";
import Layout from "containers/LayoutPanel";
import CanvasSidePanel from "components/CanvasSidePanel";
import ScreenshotModal from "components/ScreenshotModal";
import { CanvasRootProvider } from "contexts/CanvasRootContext";
import { CanvasConfProvider } from "contexts/CanvasConfContext";
import { ScreenshotsProvider } from "contexts/ScreenshotsContext";

const RootHome: React.FC = React.memo(() => {
  return (
    <React.Fragment>
      <Layout>
        <Layout.Main>
          <CanvasRoot />
        </Layout.Main>
        <Layout.Side>
          <CanvasSidePanel />
        </Layout.Side>
      </Layout>
      <ScreenshotModal />
    </React.Fragment>
  );
});

const Home: React.FC = () => {
  return (
    <CanvasRootProvider>
      <ScreenshotsProvider>
        <CanvasConfProvider>
          <RootHome />
        </CanvasConfProvider>
      </ScreenshotsProvider>
    </CanvasRootProvider>
  );
};

export default Home;
