import { makeStyles } from "@material-ui/styles";
import { Theme } from "@material-ui/core";

export default makeStyles((theme: Theme) => ({
  docPanelInner: {
    padding: theme.spacing(4),
    overflow: "auto",
    height: "100%",
    width: "100%"
  }
}));
