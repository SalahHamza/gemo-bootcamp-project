import React from "react";
import DocCard from "components/DocCard";
import useStyles from "./styles";
import DocFloaters from "components/DocCard/DocFloaters";
import LayoutPanel from "containers/LayoutPanel";
import { ReportProvider } from "contexts/ReportRootContext";

const Report: React.FC = () => {
  const classes = useStyles();
  return (
    <ReportProvider>
      <LayoutPanel>
        <LayoutPanel.Main>
          <div className={classes.docPanelInner}>
            <DocCard />
          </div>
          <DocFloaters />
        </LayoutPanel.Main>
        <LayoutPanel.Side></LayoutPanel.Side>
      </LayoutPanel>
    </ReportProvider>
  );
};

export default Report;
