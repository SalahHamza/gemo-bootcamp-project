import { RectConf } from "konva/types/Util";
import Konva from "konva";

/**
 * types
 */

export interface ShapeRectConf extends RectConf {
  type: "rect";
}
export interface ShapeCircleConf extends Konva.Vector2d {
  type: "circle";
  radius: number;
}

interface ShapeRect extends ShapeRectConf {
  id: string;
  annotation?: string;
  annotationPos: Konva.Vector2d | null;
  color: RgbaColor;
  show: boolean;
}
interface ShapeCircle extends ShapeCircleConf {
  id: string;
  annotation?: string;
  annotationPos: Konva.Vector2d | null;
  color: RgbaColor;
  show: boolean;
}

export type ShapeItem = ShapeRect | ShapeCircle;

export type ImageItem = {
  id: string;
  src: string;
};

export type ScreenItem = {
  comment?: string;
} & ImageItem;

export type Frame = ShapeItem & { image: string };

// canvas tool
export type CanvasToolItemType = "drawer:rect" | "drawer:circle" | "pointer";

export type RgbaColor = {
  r: number;
  g: number;
  b: number;
  a: number;
};
/**
 * values
 */

// default rect size, this is use when the user
// right click and adds the rect
export const RECT_SIZE = 130;

// this is the minimum height/width of a rect
export const MIN_Shape_SIZE = 20;

// canvas default scale value
// Note: the canvas scale goes from 0 to 1
export const CANVAS_DEFAULT_SCALE = 0.65;
export const CANVAS_MIN_SCALE = 0.2;
export const CANVAS_MAX_SCALE = 1.5;
export const CANVAS_ZOOM_STEP = 0.01;
export const CANVAS_IMAGE_NAME = "xr-image";
// this is how much size the image will take initially
export const CANVAS_IMAGE_RATIO = 0.95;

// used to snap shapes into a frame
export const CANVAS_SNAP_THRESHOLD = 15;

export const SHAPE_DEFAULT_COLOR = "rgba(255, 234, 0, 1)";
export const SHAPE_DEFAULT_COLOR_MAP = {
  r: 255,
  g: 234,
  b: 0,
  a: 0
} as const;

// this is used for the canvas rect name
// the name will consist of this prefix + rect.id
export const CANVAS_RECT_PREFIX = "c-rect-";
