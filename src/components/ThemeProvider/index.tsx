import React from "react";
import { ThemeProvider } from "@material-ui/styles";
import themeCreator from "styles/theme";

const apptheme = themeCreator();

const AppThemeProvider: React.FC = ({ children }) => {
  return <ThemeProvider theme={apptheme}>{children}</ThemeProvider>;
};

export default AppThemeProvider;
