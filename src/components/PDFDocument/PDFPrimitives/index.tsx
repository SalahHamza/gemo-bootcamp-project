import React from "react";
import styled from "@react-pdf/styled-components";
import { grey } from "@material-ui/core/colors";
import ReactPDF, { Page } from "@react-pdf/renderer";

/**
 *
 * transforms given unit value to pt (1unit = 8px)
 * This is useful because react-pdf supports px
 * for spacing only. and default is 'pt'
 */
const spx = (n = 1): string => `${n * 6}pt`;

export const A4Page = styled((props: ReactPDF.PageProps) => <Page size="A4" {...props} />)`
  padding: 24px 16px;
`;

export const Section = styled.View`
  padding: ${spx(2)} ${spx()};
  margin-bottom: 16px;
`;

export const SectionBox = styled.View`
  padding: ${spx(2)} ${spx()};
  border: 1pt solid ${grey[900]};
  margin-bottom: 16px;
`;

export const Title = styled.Text`
  margin-bottom: ${spx(5)};
  text-transform: uppercase;
  font-size: ${spx(2)};
  text-align: center;
  font-family: Roboto;
  font-weight: 700;
`;

export const Heading = styled.Text`
  font-size: ${spx(2.5)};
  line-height: 1;
  margin-bottom: 16px;
  text-transform: capitalize;
`;

export const SubHeading = styled.Text`
  font-weight: 900;
  font-size: ${spx(1.75)};
  line-height: 1;
  margin-bottom: 16px;
  text-transform: capitalize;
`;

export const Row = styled.View`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
`;

export const Label = styled.Text`
  font-size: ${spx(2)};
  font-family: Roboto;
  font-weight: 300;
  margin-right: 8px;
`;

export const Value = styled.Text`
  font-size: ${spx(2)};
  line-height: 1;
  margin-right: 8px;
  flex-grow: 1;
  border-bottom: 1pt dotted ${grey[400]};
`;

type FieldProps = {
  label: string;
  value: string;
} & ReactPDF.ViewProps;

export const InlineField: React.FC<FieldProps> = ({ label, value, ...rest }) => {
  return (
    <Row {...rest}>
      <Label>{label}: </Label>
      <Value>{value}</Value>
    </Row>
  );
};
