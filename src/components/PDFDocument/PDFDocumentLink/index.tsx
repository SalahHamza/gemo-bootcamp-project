import React from "react";
import { PDFDownloadLink } from "@react-pdf/renderer";
import PDFDocument from "components/PDFDocument";

const PDFDocumentLink: typeof PDFDocument = ({ data, children }) => {
  return (
    <PDFDownloadLink document={<PDFDocument data={data} />} fileName="doc-report.pdf">
      {children}
    </PDFDownloadLink>
  );
};

export default PDFDocumentLink;
