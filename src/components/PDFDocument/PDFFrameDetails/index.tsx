import React from "react";
import { Frame } from "constants/canvas";
import * as pdfPrimes from "components/PDFDocument/PDFPrimitives";
import { View, Image } from "@react-pdf/renderer";

type Props = {
  frame: Frame;
  num: number;
};

const PDFFrameDetails: React.FC<Props> = ({ frame, num }) => {
  const title = frame.annotation || "Frame";

  return (
    <pdfPrimes.Row>
      <View style={{ width: "60%" }}>
        <pdfPrimes.SubHeading>
          {num}. {title}
        </pdfPrimes.SubHeading>
      </View>
      <View style={{ width: "40%" }}>
        <Image src={frame.image} />
      </View>
    </pdfPrimes.Row>
  );
};

export default PDFFrameDetails;
