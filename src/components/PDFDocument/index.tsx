import React from "react";
import { Document } from "@react-pdf/renderer";
import * as pdfPrimes from "components/PDFDocument/PDFPrimitives";
import registerFonts from "./fonts";
import { DocData } from "contexts/ReportRootContext/reducer";

// we register the fonts for the document
registerFonts();

interface Props {
  data: DocData;
}

const PDFDocument: React.FC<Props> = ({ data }) => {
  return (
    <Document>
      <pdfPrimes.A4Page>
        <pdfPrimes.Title>Document title</pdfPrimes.Title>
        <pdfPrimes.Row style={{ justifyContent: "flex-end", marginBottom: 24 }}>
          <pdfPrimes.InlineField style={{ width: "33%" }} label="Date" value={data.date} />
        </pdfPrimes.Row>
        <pdfPrimes.Heading>Personal Information</pdfPrimes.Heading>
        <pdfPrimes.SectionBox>
          <pdfPrimes.Row>
            <pdfPrimes.InlineField
              style={{ flexGrow: 1 }}
              label="First name"
              value={data.firstName}
            />
            <pdfPrimes.InlineField
              style={{ flexGrow: 1 }}
              label="Last name"
              value={data.lastName}
            />
            <pdfPrimes.InlineField style={{ width: "20%" }} label="Sex" value={data.sex} />
          </pdfPrimes.Row>

          <pdfPrimes.Row style={{ marginTop: "16px", width: "100%" }}>
            <pdfPrimes.InlineField style={{ width: "40%" }} label="Phone" value={data.phone} />
            <pdfPrimes.InlineField style={{ width: "60%" }} label="Address" value={data.address} />
          </pdfPrimes.Row>
        </pdfPrimes.SectionBox>
      </pdfPrimes.A4Page>
    </Document>
  );
};

export default PDFDocument;
