import { Font } from "@react-pdf/renderer";

/**
 * register pdf document fonts
 */
export default (): void => {
  Font.register({
    family: "Roboto",
    fonts: [
      {
        src: "https://fonts.gstatic.com/s/roboto/v20/KFOmCnqEu92Fr1Mu4mxPKTU1Kg.ttf"
      },
      {
        src: "https://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmSU5fBBc9AMP6lQ.ttf",
        fontWeight: 300
      },
      {
        src: "https://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmEU9fBBc9AMP6lQ.ttf",
        fontWeight: 500
      },
      {
        src: "https://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmWUlfBBc9AMP6lQ.ttf",
        fontWeight: 700
      }
    ]
  });
};
