import React from "react";
import { Typography } from "@material-ui/core";

type Props = {
  index: number;
  value: number;
};

const TabPanel: React.FC<Props> = ({ children, index, value, ...rest }) => {
  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      style={{ height: "100%" }}
      {...rest}
    >
      {value === index && children}
    </Typography>
  );
};

export default TabPanel;
