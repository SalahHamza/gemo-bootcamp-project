import React from "react";
import { AppBar, Tabs, Tab, Box, Typography } from "@material-ui/core";
import TabPanel from "./TabPanel";
import ShapesPanel from "components/ShapesPanel";
import ScreenshotsPanel from "components/ScreenshotsPanel";

const tabItems = [
  {
    label: "frames",
    component: ShapesPanel
  },
  {
    label: "shots",
    component: ScreenshotsPanel
  }
];

const CanvasSidePanel: React.FC = () => {
  const [currentTab, setCurrentTab] = React.useState(0);

  const handleChange = (event, newValue) => {
    setCurrentTab(newValue);
  };

  return (
    <Box display="flex" flexDirection="column" height="100%">
      <AppBar position="static" color="default">
        <Tabs
          value={currentTab}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          scrollButtons="auto"
          aria-label="Side panel tabs"
          variant="fullWidth"
        >
          {tabItems.map(({ label }) => (
            <Tab key={label} label={<Typography variant="caption">{label}</Typography>} />
          ))}
        </Tabs>
      </AppBar>

      <Box height="100%" overflow="auto">
        {tabItems.map(({ label, component: Comp }, index) => (
          <TabPanel key={label} index={index} value={currentTab}>
            <Comp />
          </TabPanel>
        ))}
      </Box>
    </Box>
  );
};

export default CanvasSidePanel;
