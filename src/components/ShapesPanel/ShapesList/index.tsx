import React from "react";
import ShapesListItem from "components/ShapesPanel/ShapesListItem";
import { useCanvasRootCTX } from "contexts/CanvasRootContext";
import { ShapeItem, RgbaColor } from "constants/canvas";

const ShapesList: React.FC = () => {
  const { state: rootState, dispatch } = useCanvasRootCTX();

  const stageItem = rootState.current ? rootState.images[rootState.current] : null;

  const handleDeleteClick = (id: string) => (event): void => {
    // since the delete button withn the panel summary
    // that triggers open/close of panel item, we stop
    // the propagation of the event to not toggle panel item
    event.stopPropagation();
    dispatch({ type: "remove_shape", payload: id });
  };

  const handleChange = (id: string) => (event, newExpanded): void => {
    dispatch({
      type: "set_selected",
      payload: newExpanded ? id : null
    });
  };

  const handleFieldChange = (key: "annotation" | "color" | "show", item: ShapeItem) => (
    value: string | RgbaColor | boolean
  ): void => {
    const rect = {
      ...item,
      [key]: value
    };
    dispatch({ type: "update_shape", payload: rect });
  };

  return (
    <React.Fragment>
      {stageItem?.shapes.map(item => (
        <ShapesListItem
          key={item.id}
          item={item}
          onChange={handleChange(item.id)}
          expanded={item.id === stageItem?.selected}
          onDelete={handleDeleteClick(item.id)}
          onTitleChange={handleFieldChange("annotation", item)}
          onColorChange={handleFieldChange("color", item)}
          onVisibilityChange={handleFieldChange("show", item)}
        />
      ))}
    </React.Fragment>
  );
};

export default ShapesList;
