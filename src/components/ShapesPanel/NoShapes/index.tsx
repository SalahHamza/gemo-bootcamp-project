import React from "react";
import { Box, Typography } from "@material-ui/core";
import ListAddIcon from "@material-ui/icons/PlaylistAddRounded";

const NoShapes: React.FC = () => {
  return (
    <Box
      height="100%"
      width="100%"
      display="flex"
      alignItems="center"
      justifyContent="center"
      flexDirection="column"
    >
      <Typography display="block" variant="subtitle2" color="textSecondary">
        Try adding highlights.
      </Typography>
      <Typography display="block" align="center">
        <ListAddIcon fontSize="large" color="disabled" />
      </Typography>
    </Box>
  );
};

export default NoShapes;
