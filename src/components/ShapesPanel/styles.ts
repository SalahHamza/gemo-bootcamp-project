import { makeStyles } from "@material-ui/styles";
import { Theme } from "@material-ui/core";

export default makeStyles((theme: Theme) => ({
  root: {
    overflowY: "auto",
    overflow: "hidden",
    height: "100%",
    display: "flex",
    flexDirection: "column",
    position: "relative"
  },
  list: {
    flexGrow: 1,
    flexShrink: 0
  },
  action: {
    width: "100%",
    position: "sticky",
    bottom: 0,
    left: 0,
    background: theme.palette.primary.main,
    color: "white",
    textTransform: "uppercase",
    boxShadow: theme.shadows[4],
    padding: theme.spacing(1.75, 1)
  }
}));
