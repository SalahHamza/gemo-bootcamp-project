import React from "react";
import useStyles from "./styles";
import ShapesList from "components/ShapesPanel/ShapesList";
import NoShapes from "components/ShapesPanel/NoShapes";
import { ButtonBase, Typography } from "@material-ui/core";
import DescriptionIcon from "@material-ui/icons/Description";
import { useCanvasRootCTX } from "contexts/CanvasRootContext";

const ShapesPanel: React.FC = () => {
  const classes = useStyles();

  const { state: rootState } = useCanvasRootCTX();
  const stageItem = rootState.current ? rootState.images[rootState.current] : null;

  const handleReportBtnClick = (): void => {};

  return (
    <div className={classes.root}>
      <div className={classes.list}>{stageItem?.shapes.length ? <ShapesList /> : <NoShapes />}</div>
      <ButtonBase
        disabled={!rootState.stage}
        onClick={handleReportBtnClick}
        className={classes.action}
      >
        <Typography variant="button">generate report </Typography>
        <DescriptionIcon />
      </ButtonBase>
    </div>
  );
};

export default ShapesPanel;
