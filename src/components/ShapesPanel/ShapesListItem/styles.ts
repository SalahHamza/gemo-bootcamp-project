import { makeStyles } from "@material-ui/styles";
import { Theme } from "@material-ui/core";
import { red } from "@material-ui/core/colors";

export default makeStyles((theme: Theme) => ({
  fieldsWrapper: {
    width: "100%"
  },
  field: {
    marginBottom: theme.spacing(2)
  },
  colorField: {
    textAlign: "right"
  },
  deleteBtn: {
    position: "absolute",
    right: theme.spacing(0.5),
    "& svg": {
      fill: "rgba(255, 255, 255, 0.3)"
    },
    "&:hover svg, &:focus svg": {
      fill: red[400]
    }
  }
}));
