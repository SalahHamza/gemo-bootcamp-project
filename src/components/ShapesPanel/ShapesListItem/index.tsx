import React from "react";
import {
  Typography,
  IconButton,
  TextField,
  Checkbox,
  CheckboxProps,
  FormControlLabel
} from "@material-ui/core";
import * as Panel from "./Panel";
import {
  DeleteRounded as DeleteRoundedIcon,
  Visibility as VisibilityIcon,
  VisibilityOff as VisibilityOffIcon
} from "@material-ui/icons";
import useStyles from "./styles";
import { ShapeItem, RgbaColor } from "constants/canvas";
import { clsx } from "utils";
import ColorPicker from "components/ColorPicker";

const VisibilityToggle: React.FC<CheckboxProps> = props => {
  return (
    <FormControlLabel
      aria-label="toggle visiblity"
      onClick={event => event.stopPropagation()}
      onFocus={event => event.stopPropagation()}
      label=""
      control={
        <Checkbox
          {...props}
          color="default"
          icon={<VisibilityOffIcon height={18} width={18} />}
          checkedIcon={<VisibilityIcon height={18} width={18} />}
          size="small"
        />
      }
    />
  );
};

type Props = {
  expanded: boolean;
  onChange: (event: React.ChangeEvent<{}>, expanded: boolean) => void;
  onDelete: (event) => void;
  onTitleChange: (event) => void;
  onColorChange: (color: RgbaColor) => void;
  onVisibilityChange: (show: boolean) => void;
  item: ShapeItem;
};

const ShapesListItem: React.FC<Props> = ({
  expanded,
  onChange,
  onColorChange,
  item,
  onDelete,
  onTitleChange,
  onVisibilityChange
}) => {
  const classes = useStyles();

  function renderTitle(): React.ReactNode {
    if (item.annotation) return item.annotation;

    return (
      <React.Fragment>
        Frame
        <Typography variant="caption" color="textSecondary">
          #{item.id.substr(1)}
        </Typography>
      </React.Fragment>
    );
  }

  return (
    <Panel.Root square expanded={expanded} onChange={onChange} data-testid="shapes-panel-item">
      <Panel.Summary aria-controls={`${item.id}-content`} id={`${item.id}-header`}>
        <VisibilityToggle
          checked={item.show}
          onChange={(e, checked): void => onVisibilityChange(checked)}
        />
        <Typography style={{ textOverflow: "ellipsis" }} noWrap>
          {renderTitle()}
        </Typography>
        <IconButton onClick={onDelete} className={classes.deleteBtn} aria-label="delete">
          <DeleteRoundedIcon />
        </IconButton>
      </Panel.Summary>
      <Panel.Details>
        <div className={classes.fieldsWrapper}>
          <div className={clsx(classes.field, classes.colorField)}>
            <ColorPicker onChange={onColorChange} color={item.color} />
          </div>
          <div className={classes.field}>
            <TextField
              size="small"
              value={item.annotation}
              id={`${item.id}-annotation`}
              onChange={(e): void => onTitleChange(e.target.value)}
              label="Annotation"
              variant="outlined"
              fullWidth
            />
          </div>
        </div>
      </Panel.Details>
    </Panel.Root>
  );
};

export default ShapesListItem;
