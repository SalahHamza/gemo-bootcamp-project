import React from "react";
import {
  ExpansionPanel as MuiExpansionPanel,
  ExpansionPanelSummary as MuiExpansionPanelSummary,
  ExpansionPanelDetails as MuiExpansionPanelDetails,
  Checkbox,
  CheckboxProps,
  FormControlLabel,
  Theme
} from "@material-ui/core";
import VisibilityIcon from "@material-ui/icons/Visibility";
import VisibilityOffIcon from "@material-ui/icons/VisibilityOff";

import { withStyles } from "@material-ui/styles";

export const Root = withStyles({
  root: {
    boxShadow: "none",
    "&:not(:last-child)": {
      borderBottom: 0
    },
    "&:before": {
      display: "none"
    },
    "&$expanded": {
      margin: "auto"
    }
  },
  expanded: {}
})(MuiExpansionPanel);

export const Summary = withStyles({
  root: {
    paddingRight: 48,
    backgroundColor: "rgba(0, 0, 0, .03)",
    borderBottom: "1px solid rgba(0, 0, 0, .125)",
    marginBottom: -1,
    minHeight: 56,
    "&$expanded": {
      minHeight: 56
    }
  },
  content: {
    margin: 0,
    alignItems: "center",
    "&$expanded": {
      margin: 0
    }
  },
  expanded: {}
})(MuiExpansionPanelSummary);

export const Details = withStyles((theme: Theme) => ({
  root: {
    padding: theme.spacing(2),
    background: theme.palette.background.default
  }
}))(MuiExpansionPanelDetails);

export const VisibilityToggle: React.FC<CheckboxProps> = props => {
  return (
    <FormControlLabel
      aria-label="toggle visiblity"
      onClick={event => event.stopPropagation()}
      onFocus={event => event.stopPropagation()}
      label=""
      control={
        <Checkbox
          {...props}
          color="default"
          icon={<VisibilityOffIcon height={18} width={18} />}
          checkedIcon={<VisibilityIcon height={18} width={18} />}
          size="small"
        />
      }
    />
  );
};
