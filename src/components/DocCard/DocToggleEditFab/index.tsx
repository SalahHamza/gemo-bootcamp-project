import React from "react";
import { Zoom, Theme, Fab, Box } from "@material-ui/core";
import { EditOutlined, RemoveRedEyeOutlined } from "@material-ui/icons";
import { useReportContext } from "contexts/ReportRootContext";
import { useTheme } from "@material-ui/styles";

const DocToggleEditFab: React.FC = () => {
  const {
    state: { editMode },
    dispatch
  } = useReportContext();

  const theme: Theme = useTheme();

  const handleModeToggle = (): void => {
    dispatch({ type: "toggle_edit" });
  };

  const transitionDuration = {
    enter: theme.transitions.duration.enteringScreen,
    exit: theme.transitions.duration.leavingScreen
  };

  return (
    <Box position="relative" height={56} mb={1}>
      <Zoom
        in={!editMode}
        timeout={transitionDuration}
        style={{
          transitionDelay: `${!editMode ? transitionDuration.exit : 0}ms`
        }}
        unmountOnExit
      >
        <Fab
          style={{ position: "absolute" }}
          onClick={handleModeToggle}
          aria-label="edit"
          color="primary"
        >
          {<EditOutlined />}
        </Fab>
      </Zoom>
      <Zoom
        in={editMode}
        timeout={transitionDuration}
        style={{
          transitionDelay: `${editMode ? transitionDuration.exit : 0}ms`
        }}
        unmountOnExit
      >
        <Fab style={{ position: "absolute" }} onClick={handleModeToggle} aria-label="view">
          {<RemoveRedEyeOutlined />}
        </Fab>
      </Zoom>
    </Box>
  );
};

export default DocToggleEditFab;
