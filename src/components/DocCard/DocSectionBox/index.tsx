import React from "react";
import { Box } from "@material-ui/core";

const DocSectionBox: React.FC<{}> = ({ children }) => {
  return (
    <Box border={1} borderColor="grey.900" px={1} py={2} borderRadius={1}>
      {children}
    </Box>
  );
};

export default DocSectionBox;
