import React from "react";
import { Box } from "@material-ui/core";
import DocToggleEditFab from "components/DocCard/DocToggleEditFab";
import DocPrintFab from "components/DocCard/DocPrintFab";

const DocFloaters: React.FC = () => {
  return (
    <Box position="absolute" display="flex" flexDirection="column" right={24} bottom={16}>
      <DocToggleEditFab />
      <DocPrintFab />
    </Box>
  );
};

export default DocFloaters;
