import React from "react";
import DocField from "components/DocCard/DocField";
import { useReportContext } from "contexts/ReportRootContext";
import DocInput from "components/DocCard/DocInput";

type Props = {
  label: string;
  value?: string;
  onChange?: (value: string) => void;
  type?: string;
  defaultValue?: string;
  text?: string;
};

const DocFieldInput: React.FC<Props> = ({ label, value, text = value, onChange, ...rest }) => {
  const {
    state: { editMode }
  } = useReportContext();

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    if (onChange) onChange(event.target.value);
  };

  function renderContent(): React.ReactNode {
    if (!editMode) return text;
    return <DocInput {...rest} defaultValue={value} onChange={handleChange} fullWidth />;
  }

  return <DocField label={label}>{renderContent()}</DocField>;
};

export default DocFieldInput;
