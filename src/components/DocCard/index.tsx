import React from "react";
import DocPaper from "components/DocCard/DocPaper";
import { Typography, Box, Grid } from "@material-ui/core";
import useStyles from "./styles";
import DocFieldInput from "components/DocCard/DocFieldInput";
import DocFieldSelect, { SelectOptions } from "components/DocCard/DocFieldSelect";
import DocSectionBox from "components/DocCard/DocSectionBox";
import { useReportContext } from "contexts/ReportRootContext";
import dayjs from "dayjs";
import { DocData } from "contexts/ReportRootContext/reducer";

export const SEXES: SelectOptions = [
  { value: "male", label: "Male" },
  { value: "Female", label: "Female" }
];

const ReportCard: React.FC = () => {
  const classes = useStyles();

  const {
    dispatch,
    state: { values }
  } = useReportContext();

  const handleValueChange = (obj: { key: keyof DocData; value: string }): void => {
    dispatch({ type: "set_value", payload: obj });
  };

  return (
    <React.Fragment>
      <DocPaper>
        <Box textAlign="center" mb={4}>
          <Typography className={classes.title} variant="h6" component="h1" gutterBottom>
            Document Title
          </Typography>
        </Box>
        <Grid container justify="flex-end" style={{ marginBottom: 24 }}>
          <Grid item xs={4}>
            <DocFieldInput
              type="date"
              defaultValue={dayjs(values.date).format("YYYY-MM-DD")}
              text={dayjs(values.date).format("MM/DD/YYYY")}
              onChange={(value): void => {
                handleValueChange({ value, key: "date" });
              }}
              label="Date"
            />
          </Grid>
        </Grid>
        <Typography variant="h6" component="h2" gutterBottom>
          Patient Information
        </Typography>
        <DocSectionBox>
          <Grid container spacing={2}>
            <Grid item xs={5}>
              <DocFieldInput
                value={values.firstName}
                onChange={(value): void => {
                  handleValueChange({ value, key: "firstName" });
                }}
                label="First Name"
              />
            </Grid>
            <Grid item xs={5}>
              <DocFieldInput
                value={values.lastName}
                onChange={(value): void => {
                  handleValueChange({ value, key: "lastName" });
                }}
                label="Last Name"
              />
            </Grid>
            <Grid item xs={2}>
              <DocFieldSelect
                value={values.sex}
                onChange={(value): void => {
                  handleValueChange({ value, key: "sex" });
                }}
                label="Sex"
                options={SEXES}
              />
            </Grid>
          </Grid>
          <Grid style={{ marginTop: 8 }} container spacing={2}>
            <Grid item xs={5}>
              <DocFieldInput
                value={values.phone}
                onChange={(value): void => {
                  handleValueChange({ value, key: "phone" });
                }}
                label="Phone"
              />
            </Grid>
            <Grid item xs={7}>
              <DocFieldInput
                value={values.address}
                onChange={(value): void => {
                  handleValueChange({ value, key: "address" });
                }}
                label="Address"
              />
            </Grid>
          </Grid>
        </DocSectionBox>
      </DocPaper>
    </React.Fragment>
  );
};

export default ReportCard;
