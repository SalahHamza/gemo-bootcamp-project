import React from "react";
import DocField from "components/DocCard/DocField";
import { Select, MenuItem } from "@material-ui/core";
import useStyles from "./styles";
import { useReportContext } from "contexts/ReportRootContext";

type SelectOption = { value: string | number; label: string | number };

export type SelectOptions = SelectOption[];

type Props = {
  label: string;
  options: SelectOptions;
  onChange: (value: string) => void;
  value: string | number;
};

const DocFieldSelect: React.FC<Props> = ({ label, options, onChange, value }) => {
  const classes = useStyles();

  const {
    state: { editMode }
  } = useReportContext();

  const handleChange = (event): void => {
    onChange(event.target.value);
  };

  const _selected = options.find(o => o.value === value);

  function renderContent(): React.ReactNode {
    if (!editMode) return _selected?.label;
    return (
      <Select
        onChange={handleChange}
        classes={{
          root: classes.input
        }}
        value={_selected?.value}
        defaultValue={options[0].value}
        fullWidth
      >
        {options.map(option => (
          <MenuItem key={option.value} value={option.value}>
            {option.label}
          </MenuItem>
        ))}
      </Select>
    );
  }

  return <DocField label={label}>{renderContent()}</DocField>;
};

export default DocFieldSelect;
