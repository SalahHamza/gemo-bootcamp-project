import { Theme } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";

interface Props {
  label: string;
}

export default makeStyles((theme: Theme) => ({
  root: {},
  input: {
    padding: theme.spacing(0)
  }
}));
