import React from "react";
import { Input, Typography, InputProps } from "@material-ui/core";
import useStyles from "./styles";

type Props = InputProps;

const DocInput: React.FC<Props> = props => {
  const classes = useStyles();

  return (
    <Input
      {...props}
      classes={{
        input: classes.input
      }}
      endAdornment={
        <Typography aria-hidden={true} variant="caption" color="primary">
          T
        </Typography>
      }
      fullWidth
    />
  );
};

export default DocInput;
