import { Theme } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";

export default makeStyles((theme: Theme) => ({
  root: {
    margin: `${theme.spacing(3)}px auto`,
    boxShadow: theme.shadows[5],
    // These are the ratios for an A4 paper
    // check here for more details:
    // https://stackoverflow.com/questions/3341485/how-to-make-a-html-page-in-a4-paper-size-pages
    width: "21cm",
    height: "29cm",
    padding: theme.spacing(3, 2),
    borderRadius: 0
  }
}));
