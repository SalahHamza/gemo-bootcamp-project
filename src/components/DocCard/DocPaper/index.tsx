import React from "react";
import { Paper, ThemeProvider } from "@material-ui/core";
import useStyles from "./styles";
import themeCreator from "styles/theme";
// we create light theme for document paper
const _theme = themeCreator("light");

const DocPaper: React.FC = ({ children }) => {
  const classes = useStyles();
  return (
    <ThemeProvider theme={_theme}>
      <Paper className={classes.root}>{children}</Paper>
    </ThemeProvider>
  );
};

export default DocPaper;
