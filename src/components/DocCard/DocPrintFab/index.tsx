import React, { Suspense } from "react";
import { Fab, CircularProgress } from "@material-ui/core";
import { PrintOutlined } from "@material-ui/icons";
import { useReportContext } from "contexts/ReportRootContext";

// lazy load document compoent
// NOTE: need to make it dynamic incase
// we want setup multiple templates.
const PDFDocumentLink = React.lazy(() =>
  import(/* webpackChunkName: "PDFDocumentLink" */ "components/PDFDocument/PDFDocumentLink")
);

const DocPrintFab: React.FC = () => {
  const {
    state: { values }
  } = useReportContext();

  return (
    <Suspense
      fallback={
        <Fab aria-label="download" color="primary">
          <CircularProgress size={24} color="secondary" />
        </Fab>
      }
    >
      <PDFDocumentLink data={values}>
        {({ loading }): JSX.Element => (
          <Fab aria-label="download" color="primary">
            {loading ? <CircularProgress size={24} color="secondary" /> : <PrintOutlined />}
          </Fab>
        )}
      </PDFDocumentLink>
    </Suspense>
  );
};

export default DocPrintFab;
