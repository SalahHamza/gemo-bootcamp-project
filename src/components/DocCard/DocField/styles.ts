import { makeStyles } from "@material-ui/styles";
import { Theme } from "@material-ui/core";

export default makeStyles((theme: Theme) => ({
  root: { width: "100%" },
  innerWrapper: {
    display: "flex",
    alignItems: "baseline"
  },
  label: {
    fontWeight: 500
  },
  field: {
    lineHeight: 1,
    marginLeft: theme.spacing(1),
    flexGrow: 1,
    display: "inline-block",
    position: "relative",
    "&:after": {
      content: "' '",
      position: "absolute",
      width: "100%",
      bottom: 0,
      left: 0,
      borderBottom: `1px dotted ${theme.palette.grey[500]}`
    }
  }
}));
