import React from "react";
import useStyles from "./styles";
import { Typography } from "@material-ui/core";

interface Props {
  label: string;
  children?: React.ReactNode;
}

const DocField: React.FC<Props> = ({ label, children }) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <label className={classes.innerWrapper}>
        <Typography className={classes.label} display="inline" variant="subtitle1">
          {label}:
        </Typography>
        <Typography variant="subtitle1" className={classes.field}>
          {children}
        </Typography>
      </label>
    </div>
  );
};

export default DocField;
