import { makeStyles } from "@material-ui/styles";
import { Theme } from "@material-ui/core";

export default makeStyles((theme: Theme) => ({
  title: {
    textTransform: "uppercase",
    padding: theme.spacing(0.5, 1),
    fontWeight: 700,
    display: "inline-block"
  },
  img: {
    width: "100%"
  }
}));
