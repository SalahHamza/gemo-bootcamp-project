import { makeStyles } from "@material-ui/styles";
import { Theme } from "@material-ui/core";

export default makeStyles((theme: Theme) => ({
  root: {
    position: "relative",
    display: "inline-block"
  },
  btn: {
    padding: theme.spacing(0.5),
    background: theme.palette.background.paper,
    borderRadius: 1,
    boxShadow: "0 0 0 1px rgba(0,0,0,.1)",
    display: "inline-block",
    cursor: "pointer"
  },
  color: {
    width: 36,
    height: 14,
    borderRadius: 2
  },
  popover: {
    position: "absolute",
    zIndex: 9,
    right: 0
  },
  cover: {
    position: "fixed",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  },
  picker: {}
}));
