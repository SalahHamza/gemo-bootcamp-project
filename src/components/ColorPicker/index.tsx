import React from "react";
import { ChromePicker, ColorResult } from "react-color";
import useStyles from "./styles";
import { ButtonBase, ButtonBaseTypeMap, createStyles, useTheme } from "@material-ui/core";
import { RgbaColor } from "constants/canvas";
import { getRgbaString } from "utils/canvas";
import { ChromePickerStyles } from "react-color/lib/components/chrome/Chrome";

type Props = {
  btnProps?: ButtonBaseTypeMap;
  color: RgbaColor;
  onChange: (color: RgbaColor) => void;
};

/**
 * @todo add throttling to the onChange event handler
 */
const ColorPicker: React.FC<Props> = ({ btnProps = {}, color, onChange }) => {
  const classes = useStyles();
  const [show, setShow] = React.useState(false);

  const handleChange = (newColor: ColorResult): void => {
    onChange(newColor.rgb as RgbaColor);
  };

  const theme = useTheme();

  const pickerStyles = createStyles({
    default: {
      body: {
        background: theme.palette.background.default
      }
    }
  }) as ChromePickerStyles;

  return (
    <div className={classes.root}>
      <ButtonBase onClick={(): void => setShow(true)} className={classes.btn} {...btnProps}>
        <div className={classes.color} style={{ backgroundColor: getRgbaString(color) }}></div>
      </ButtonBase>
      {show ? (
        <div className={classes.popover}>
          <div className={classes.cover} onClick={(): void => setShow(false)}></div>
          <ChromePicker styles={pickerStyles} onChange={handleChange} color={color} />
        </div>
      ) : null}
    </div>
  );
};

export default ColorPicker;
