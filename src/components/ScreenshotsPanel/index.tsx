import React from "react";
import { GridListTile, IconButton, GridList } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/DeleteRounded";
import { useScreenshotsCTX } from "contexts/ScreenshotsContext";
import { ScreenItem } from "constants/canvas";
import useStyles from "./styles";
import NoScreenshots from "./NoScreenshots";

const ScreenshotsPanel: React.FC = () => {
  const classes = useStyles();
  const containerRef = React.useRef<HTMLDivElement | null>(null);
  const { dispatch: ssDispatch, state: ssState } = useScreenshotsCTX();

  const handleItemRemove = (shot: ScreenItem) => event => {
    event.stopPropagation();
    ssDispatch({ type: "remove_screenshot", payload: shot.id });
  };

  const handleItemClick = (shot: ScreenItem) => () => {
    ssDispatch({ type: "open_edit_modal", payload: shot.id });
  };

  function renderList() {
    if (!ssState.items.length) {
      return <NoScreenshots />;
    }

    return ssState.items.map(shot => (
      <GridListTile
        onClick={handleItemClick(shot)}
        tabIndex={0}
        key={shot.id}
        className={classes.item}
        classes={{ tile: classes.itemTile }}
        cols={1}
      >
        <img src={shot.src} alt="" />
        <IconButton
          onClick={handleItemRemove(shot)}
          className={classes.removeBtn}
          size="small"
          aria-label="Delete screenshot"
        >
          <DeleteIcon />
        </IconButton>
      </GridListTile>
    ));
  }

  return (
    <div ref={containerRef} className={classes.root}>
      <GridList cellHeight={100} cols={3}>
        {renderList()}
      </GridList>
    </div>
  );
};

export default ScreenshotsPanel;
