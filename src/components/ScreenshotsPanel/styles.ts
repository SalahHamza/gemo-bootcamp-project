import { makeStyles } from "@material-ui/styles";
import { Theme } from "@material-ui/core";

export default makeStyles((theme: Theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-around",
    overflow: "hidden",
    position: "relative",
    height: "100%"
  },
  item: {
    opacity: 0.7,
    cursor: "pointer",
    "&:hover $removeBtn, &:focus ": {
      display: "initial"
    },
    "&:hover, &:focus": {
      opacity: 1,
      outline: "none",
      boxShadow: "none"
    },
    "& img": {
      height: "100%",
      maxWidth: "100%",
      objectFit: "contain"
    }
  },
  itemTile: {
    boxShadow: theme.shadows[3],
    background: theme.palette.common.black
  },
  removeBtn: {
    display: "none",
    position: "absolute",
    bottom: theme.spacing(0.25),
    right: theme.spacing(0.25),
    "& svg": {
      fill: theme.palette.error.light
    }
  }
}));
