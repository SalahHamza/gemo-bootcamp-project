import React from "react";
import { Box, Typography } from "@material-ui/core";
import CameraIcon from "@material-ui/icons/CameraAltRounded";

const NoScreenshots: React.FC = () => {
  return (
    <Box
      height="100%"
      width="100%"
      display="flex"
      alignItems="center"
      justifyContent="center"
      flexDirection="column"
    >
      <Typography display="block" variant="subtitle2" color="textSecondary">
        Try taking some screeshots
      </Typography>
      <Typography display="block" align="center">
        <CameraIcon fontSize="large" color="disabled" />
      </Typography>
    </Box>
  );
};

export default NoScreenshots;
