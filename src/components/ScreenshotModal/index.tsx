import React from "react";
import { Dialog, CircularProgress, Box } from "@material-ui/core";
import { useScreenshotsCTX } from "contexts/ScreenshotsContext";

// we lazyload the content
const AddModal = React.lazy(() => import(/* webpackChunkName: "AddModal" */ "./AddModal"));
const EditModal = React.lazy(() => import(/* webpackChunkName: "EditModal" */ "./EditModal"));

const Loading = () => {
  return (
    <Box height={300} display="flex" alignItems="center" justifyContent="center">
      <CircularProgress color="primary" />
    </Box>
  );
};

const ScreenshotModal: React.FC = () => {
  const { dispatch: ssDispatch, state: ssState } = useScreenshotsCTX();

  function renderModalContent() {
    switch (ssState.modal) {
      case "add":
        return <AddModal />;
      case "edit":
        return <EditModal />;
      default:
        return <div />;
    }
  }

  return (
    <Dialog
      style={{ minHeight: 300 }}
      open={ssState.modal !== null}
      onClose={() => {
        ssDispatch({ type: "close_modal" });
      }}
      maxWidth="sm"
      fullWidth
    >
      <React.Suspense fallback={<Loading />}>{renderModalContent()}</React.Suspense>
    </Dialog>
  );
};

export default ScreenshotModal;
