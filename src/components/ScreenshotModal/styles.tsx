import { makeStyles } from "@material-ui/styles";
import { Theme } from "@material-ui/core";

export default makeStyles((theme: Theme) => ({
  screenshot: {
    height: 300,
    width: "100%",
    textAlign: "center",
    background: theme.palette.grey[900],
    "& img": {
      background: theme.palette.common.black,
      height: "100%",
      maxWidth: "100%"
    }
  }
}));
