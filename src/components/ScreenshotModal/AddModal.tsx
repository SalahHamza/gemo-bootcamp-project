import React from "react";
import { DialogActions, DialogContent, Button, TextField } from "@material-ui/core";
import useStyles from "./styles";
import { uniqId } from "utils";
import { useScreenshotsCTX } from "contexts/ScreenshotsContext";

const AddModal: React.FC = () => {
  const classes = useStyles();
  const { dispatch: ssDispatch, state: ssState } = useScreenshotsCTX();

  const closeModal = () => {
    ssDispatch({ type: "close_modal" });
  };

  // NOTE: make sure to debounce this function
  const handleCommentChange = event => {
    ssDispatch({
      type: "update_draft",
      payload: event.target.value
    });
  };

  const handleAddClick = () => {
    if (!ssState?.draft?.src) return;

    ssDispatch({
      type: "add_screenshot",
      payload: {
        id: uniqId(),
        ...ssState.draft
      }
    });
  };
  return (
    <React.Fragment>
      <div className={classes.screenshot}>
        {ssState?.draft?.src ? <img src={ssState?.draft?.src} alt="" /> : null}
      </div>
      <DialogContent>
        <TextField
          size="small"
          id="screenshot-modal-comment"
          onChange={handleCommentChange}
          label="Comment"
          variant="outlined"
          helperText="Comment to include in the report"
          rows={4}
          multiline
          fullWidth
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={closeModal} size="small" color="primary">
          Cancel
        </Button>
        <Button onClick={handleAddClick} size="small" color="primary" variant="contained">
          Add
        </Button>
      </DialogActions>
    </React.Fragment>
  );
};

export default AddModal;
