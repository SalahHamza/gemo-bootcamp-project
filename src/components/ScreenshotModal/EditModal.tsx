import React from "react";
import { DialogActions, DialogContent, Button, TextField } from "@material-ui/core";
import useStyles from "./styles";
import { useScreenshotsCTX } from "contexts/ScreenshotsContext";

const EditModal: React.FC = () => {
  const classes = useStyles();
  const { dispatch: ssDispatch, state: ssState } = useScreenshotsCTX();

  const currentShot = React.useMemo(() => {
    return ssState.items.find(s => s.id === ssState.current);
  }, [ssState]);

  const [value, setValue] = React.useState(currentShot?.comment);

  const closeModal = () => {
    ssDispatch({ type: "close_modal" });
  };

  const handleCommentChange = event => {
    setValue(event.target.value);
  };

  const handleUpdateClick = () => {
    if (!currentShot) return;

    ssDispatch({
      type: "update_screenshot",
      payload: {
        ...currentShot,
        comment: value
      }
    });
  };

  return (
    <React.Fragment>
      <div className={classes.screenshot}>
        {currentShot?.src ? <img src={currentShot.src} alt="" /> : null}
      </div>
      <DialogContent>
        <TextField
          size="small"
          id="screenshot-modal-comment"
          value={value}
          onChange={handleCommentChange}
          label="comment"
          variant="outlined"
          helperText="Comment to include in the report"
          rows={4}
          multiline
          fullWidth
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={closeModal} size="small" color="primary">
          Cancel
        </Button>
        <Button
          disabled={value?.trim() === currentShot?.comment?.trim()}
          onClick={handleUpdateClick}
          size="small"
          color="primary"
          variant="contained"
        >
          Update
        </Button>
      </DialogActions>
    </React.Fragment>
  );
};

export default EditModal;
