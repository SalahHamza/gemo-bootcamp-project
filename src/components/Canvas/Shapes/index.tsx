import React from "react";
import { Rect, Circle } from "react-konva";
import Konva from "konva";

export const CanvasRect: React.FC<Konva.RectConfig> = React.forwardRef<
  Konva.Rect,
  Konva.RectConfig
>((props, ref) => {
  return (
    <Rect
      {...props}
      ref={ref}
      // preventing the selection of an element right click
      onContextMenu={(e): void => e.evt.preventDefault()}
      strokeWidth={4}
    />
  );
});

export const CanvasCircle: React.FC<Konva.CircleConfig> = React.forwardRef<
  Konva.Circle,
  Konva.CircleConfig
>((props, ref) => {
  return (
    <Circle
      {...props}
      ref={ref}
      // preventing the selection of an element right click
      onContextMenu={(e): void => e.evt.preventDefault()}
      strokeWidth={4}
    />
  );
});
