import { makeStyles } from "@material-ui/styles";
import { Theme } from "@material-ui/core";

export default makeStyles<Theme>(theme => ({
  btn: {
    width: "100%",
    height: 80,
    borderRadius: 2,
    marginBottom: theme.spacing(1),
    background: theme.palette.background.paper,
    border: `1px dashed ${theme.palette.divider}`,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    "& svg": {
      fill: theme.palette.divider
    }
  },
  input: {
    display: "none"
  }
}));
