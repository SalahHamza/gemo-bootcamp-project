import React from "react";
import { ButtonBase, CircularProgress } from "@material-ui/core";
import AddRoundedIcon from "@material-ui/icons/AddRounded";
import useStyles from "./styles";
import { uniqId } from "utils";
import { useCanvasRootCTX } from "contexts/CanvasRootContext";

const ImageTabUpload: React.FC = () => {
  const classes = useStyles();
  const { dispatch: rootDispatch } = useCanvasRootCTX();
  const [loading, setLoading] = React.useState(false);

  const handleImageUpload = (event): void => {
    const file = event.target.files[0];
    if (!file) return;
    const fileReader = new FileReader();

    fileReader.addEventListener("loadstart", () => {
      setLoading(true);
    });

    fileReader.addEventListener("load", () => {
      rootDispatch({
        type: "add_img",
        payload: {
          id: uniqId(),
          src: fileReader.result as string
        }
      });
    });
    fileReader.addEventListener("loadend", () => {
      // Note: this is just to display the spinner.
      // need to remove it later
      setTimeout(() => {
        setLoading(false);
      }, 850);
    });

    fileReader.addEventListener("onabort", () => {
      setLoading(false);
    });
    fileReader.addEventListener("onerror", () => {
      setLoading(false);
    });

    fileReader.readAsDataURL(file);
  };

  return (
    <React.Fragment>
      <label htmlFor="add-image-input_tab">
        <ButtonBase component="span" aria-label="add image" className={classes.btn}>
          {loading ? <CircularProgress size={30} /> : <AddRoundedIcon />}
        </ButtonBase>
      </label>

      <input
        accept="image/*"
        className={classes.input}
        id="add-image-input_tab"
        onChange={handleImageUpload}
        type="file"
      />
    </React.Fragment>
  );
};

export default ImageTabUpload;
