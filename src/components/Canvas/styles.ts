import { makeStyles } from "@material-ui/styles";
import { Theme } from "@material-ui/core";
import { CanvasToolItemType } from "constants/canvas";

type StyleProps = {
  cursor: CanvasToolItemType;
  grabbing: boolean;
};

export default makeStyles((theme: Theme) => ({
  canvasStage: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    cursor: (props: StyleProps): string => {
      if (props.grabbing) return "grab";

      return props.cursor === "drawer:rect" ? "crosshair" : "initial";
    },
    width: "100%",
    height: "calc(100vh - 56px)",
    [theme.breakpoints.up("sm")]: {
      height: "calc(100vh - 64px)"
    }
  }
}));
