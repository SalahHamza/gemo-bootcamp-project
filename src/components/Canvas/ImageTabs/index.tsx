import React from "react";
import { Paper, ButtonBase } from "@material-ui/core";
import KeyboardArrowLeftIcon from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRightIcon from "@material-ui/icons/KeyboardArrowRight";
import useStyles from "./styles";
import { clsx } from "utils";
import { useCanvasRootCTX } from "contexts/CanvasRootContext";
import ImageTabItem from "components/Canvas/ImageTabItem";
import ImageTabUpload from "components/Canvas/ImageTabUpload";

const ImageTabs: React.FC = () => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const { dispatch: rootDispatch, state: rootState } = useCanvasRootCTX();

  const handleImageClick = (id: string) => (event): void => {
    rootDispatch({
      type: "set_current_img",
      payload: id
    });
  };
  const handleImageDelete = (id: string) => (event): void => {
    event.stopPropagation();
    rootDispatch({
      type: "delete_img",
      payload: id
    });
  };

  return (
    <div className={clsx(classes.root, open && classes.opened)}>
      {open ? (
        <ButtonBase
          id="image-tab-btn"
          onClick={(): void => setOpen(false)}
          aria-label="close image tabs"
          className={classes.toggler}
        >
          <KeyboardArrowRightIcon />
        </ButtonBase>
      ) : (
        <ButtonBase
          id="image-tab-btn"
          onClick={(): void => setOpen(true)}
          aria-label="open image tabs"
          className={classes.toggler}
        >
          <KeyboardArrowLeftIcon />
        </ButtonBase>
      )}

      <Paper elevation={open ? 6 : 0} aria-hidden={!open} className={classes.card}>
        <ImageTabUpload />
        {Object.entries(rootState.images).map(([imgId, img]) => (
          <ImageTabItem
            key={imgId}
            isCurrent={imgId === rootState.current}
            src={img.src}
            onClick={handleImageClick(img.id)}
            onDelete={handleImageDelete(img.id)}
          />
        ))}
      </Paper>
    </div>
  );
};

export default React.memo(ImageTabs);
