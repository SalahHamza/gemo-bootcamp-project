import { makeStyles } from "@material-ui/styles";
import { Theme } from "@material-ui/core";

export default makeStyles<Theme>(theme => ({
  root: {
    position: "absolute",
    right: theme.spacing(0.5),
    top: theme.spacing(2),
    height: "80%",
    transform: `translateX(calc(100% + ${theme.spacing(0.5)}px))`,
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.enteringScreen,
      easing: theme.transitions.easing.easeOut
    }),
    transitionDuration: theme.transitions.duration.enteringScreen + "ms",
    "&$opened": {
      transition: theme.transitions.create("transform", {
        duration: theme.transitions.duration.leavingScreen,
        easing: theme.transitions.easing.easeIn
      }),
      transform: `translateX(0)`
    }
  },
  card: {
    height: "100%",
    borderRadius: 2,
    padding: theme.spacing(0.75),
    width: 90,
    overflowY: "auto",
    "&::-webkit-scrollbar": {
      display: "none"
    }
  },
  toggler: {
    boxShadow: theme.shadows[6],
    borderRadius: 2,
    width: 30,
    padding: theme.spacing(0.75),
    position: "absolute",
    right: `calc(100% + ${theme.spacing(0.5)}px)`,
    background: theme.palette.background.paper,
    "& svg": {
      fill: theme.palette.common.white,
      height: 20,
      width: 20
    }
  },
  opened: {}
}));
