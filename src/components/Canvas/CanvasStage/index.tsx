import React from "react";
import Konva from "konva";
import { Stage, Layer, Image, Circle } from "react-konva";
import { fade as fadeColor } from "@material-ui/core/styles/colorManipulator";
import { createNewShape, getRgbaString, isValidShapeSize } from "utils/canvas";
import {
  CANVAS_MAX_SCALE,
  CANVAS_MIN_SCALE,
  CANVAS_ZOOM_STEP,
  CANVAS_IMAGE_NAME,
  SHAPE_DEFAULT_COLOR_MAP,
  CANVAS_IMAGE_RATIO
} from "constants/canvas";
import ShapesGroup from "components/Canvas/ShapesGroup";
import { CanvasRect } from "components/Canvas/Shapes";
// contexts
import { useCanvasRootCTX, CanvasRootBridgeProvider } from "contexts/CanvasRootContext";
import { useCanvasConfCTX, CanvasConfBridgeProvider } from "contexts/CanvasConfContext";
import useCanvasShapeDraw from "hooks/canvas/useCanvasShapeDraw";

type Props = {
  height: number;
  width: number;
  imageEl?: HTMLImageElement;
};

const CanvasStage: React.FC<Props> = props => {
  const imageNodeRef = React.useRef<Konva.Image>(null);

  // we don't destructure directly because we want to
  // pass the value to the bridge context.
  const canvasRootValue = useCanvasRootCTX();
  const { dispatch: rootDispatch, state: cRootState } = canvasRootValue;

  const canvasConfValue = useCanvasConfCTX();
  const { dispatch: confDispatch, state: confState } = canvasConfValue;

  const canvasItem = cRootState.current ? cRootState.images[cRootState.current as string] : null;

  const drawer = useCanvasShapeDraw();

  // we are offsetting the is image
  // by half it's height and width
  // this will make sure the image position
  // starts at its center and not top-left corner.
  const imageOffset = {
    x: (props.imageEl?.width || 0) * 0.5,
    y: (props.imageEl?.height || 0) * 0.5
  } as const;

  /**
   * initializers
   */

  React.useEffect(() => {
    if (!props.imageEl) return;

    // we set the scale so that the image takes
    // 90% of height or width of the canvas initially.
    let initialScale: number;
    if (props.imageEl.height >= props.imageEl.width) {
      initialScale = (props.height * CANVAS_IMAGE_RATIO) / props.imageEl.height;
    } else {
      initialScale = (props.width * CANVAS_IMAGE_RATIO) / props.imageEl.width;
    }

    // these two will be batched since they are not async
    rootDispatch({ type: "set_stage_scale", payload: initialScale });
  }, [props.height, props.imageEl, props.width, rootDispatch]);

  React.useEffect(() => {
    if (canvasItem?.stageX || canvasItem?.stageY) return;
    // stage x/y initial value is undefined
    // so we fallback to its center.
    const initialPos = {
      x: props.width * 0.5,
      y: props.height * 0.5
    };
    rootDispatch({ type: "set_stage_pos", payload: initialPos });
  }, [canvasItem, props.height, props.width, rootDispatch]);

  const handleStageClick = (event): void => {
    // since handleContextMenu (right click)
    // is also a click event, we are checking
    // if the event is a normal (left) click event
    // check here for more details:
    // https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/buttons
    if (event.evt.button < 2) {
      // only update if not on grab screen
      // and there is actually a selected rect
      if (!confState.canGrab && canvasItem?.selected) {
        // we unselect the currently selected rect
        rootDispatch({ type: "set_selected", payload: null });
      }
    }
  };

  const handleStageMouseDown = (event): void => {
    // since handleContextMenu (right click)
    // is also a click event, we are checking
    // if the event is a normal (left) click event
    // check here for more details:
    // https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/buttons
    if (event.evt.button < 2) {
      drawer?.onDrawStart(event);
    }
  };

  const handleStageMouseMove = (event): void => {
    drawer?.onDrawing(event);
  };

  const handleStageMouseUp = (event): void => {
    drawer?.onDrawEnd(event);
    if (!drawer?.shape || !drawer.shape.valid) return;
    const { valid, ...shapeConf } = drawer.shape;

    const shape = createNewShape(shapeConf);

    if (!valid || !isValidShapeSize(shape)) return;

    rootDispatch({ type: "add_shape", payload: shape });
    confDispatch({ type: "set_cursor", payload: "pointer" });
  };

  /**
   *
   * @todo: use requestAnimationFrame()
   * adapted from here:
   * https://konvajs.org/docs/sandbox/Zooming_Relative_To_Pointer.html
   */
  const handleStageWheel = (event): void => {
    event.evt.preventDefault();

    // since this is a wheel zoom, we want it
    // to look a bit smoother so we devide it by 10.
    // NOTE: CANVAS_ZOOM_STEP might default later to 0.01
    // instead of 0.1
    const scaleBy = CANVAS_ZOOM_STEP;
    const stage = event.target.getStage();
    const oldScale = stage.scaleX();

    const mousePointTo = {
      x: stage.getPointerPosition().x / oldScale - stage.x() / oldScale,
      y: stage.getPointerPosition().y / oldScale - stage.y() / oldScale
    };

    const newScale = event.evt.deltaY > 0 ? oldScale + scaleBy : oldScale - scaleBy;

    // we stop zoom in/out if we reach the max/min scale values
    if (newScale > CANVAS_MAX_SCALE || newScale < CANVAS_MIN_SCALE) return;

    const x = -(mousePointTo.x - stage.getPointerPosition().x / newScale) * newScale;
    const y = -(mousePointTo.y - stage.getPointerPosition().y / newScale) * newScale;

    // since this is an event, react will batch these two changes
    // check here for details:
    // https://github.com/facebook/react/issues/14259
    // Alternatively we can do that manually by creating an action
    // that changes both stage position and stage scale.
    rootDispatch({ type: "set_stage_scale", payload: newScale });
    rootDispatch({ type: "set_stage_pos", payload: { x, y } });
  };

  const handleStageDragEnd = event => {
    const stage = event.target as Konva.Stage;

    const pos = stage.getPosition();
    rootDispatch({ type: "set_stage_pos", payload: pos });
  };

  const setStageNode = (node): void => {
    // if no node exists or it has already been set
    // there is no need to set it.
    if (!node || cRootState.stage) return;
    rootDispatch({ type: "set_stage_node", payload: node });
  };

  function renderDrawedShape() {
    if (confState.canGrab || !drawer?.shape) return null;
    const { valid, ...shape } = drawer.shape;
    const color = valid ? getRgbaString(SHAPE_DEFAULT_COLOR_MAP) : "#F00";
    const fill = fadeColor(color, SHAPE_DEFAULT_COLOR_MAP.a);
    switch (shape.type) {
      case "rect":
        return <CanvasRect {...shape} fill={fill} stroke={color} />;
      case "circle":
        return (
          <Circle
            x={shape.x}
            y={shape.y}
            radius={Math.abs(shape.radius)}
            fill={fill}
            stroke={color}
            strokeWidth={4}
            draggable
          />
        );
      default:
        return null;
    }
  }

  return (
    <Stage
      onClick={handleStageClick}
      onMouseDown={handleStageMouseDown}
      onMouseUp={handleStageMouseUp}
      onMouseMove={handleStageMouseMove}
      onWheel={handleStageWheel}
      onDragEnd={handleStageDragEnd}
      height={props.height}
      width={props.width}
      x={canvasItem?.stageX}
      y={canvasItem?.stageY}
      scaleX={canvasItem?.scale}
      scaleY={canvasItem?.scale}
      draggable={confState.canGrab}
      ref={setStageNode}
    >
      <CanvasRootBridgeProvider value={canvasRootValue}>
        <CanvasConfBridgeProvider value={canvasConfValue}>
          <Layer>
            <Image
              ref={imageNodeRef}
              offset={imageOffset}
              name={CANVAS_IMAGE_NAME}
              image={props.imageEl}
            />
          </Layer>
          <Layer>
            <ShapesGroup />

            {renderDrawedShape()}
          </Layer>
        </CanvasConfBridgeProvider>
      </CanvasRootBridgeProvider>
    </Stage>
  );
};

export default CanvasStage;
