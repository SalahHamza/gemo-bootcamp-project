import React from "react";
import { Typography, Box, Paper, Button } from "@material-ui/core";
import { useCanvasRootCTX } from "contexts/CanvasRootContext";
import { uniqId } from "utils";

const InitialImageUpload: React.FC = () => {
  const { dispatch: rootDispatch } = useCanvasRootCTX();

  const handleImageUpload = (event): void => {
    const file = event.target.files[0];
    if (!file) return;
    const fileReader = new FileReader();

    fileReader.addEventListener("load", () => {
      rootDispatch({
        type: "add_img",
        payload: {
          id: uniqId(),
          src: fileReader.result as string
        }
      });
    });

    fileReader.readAsDataURL(file);
  };

  return (
    <Box px={6} py={2} component={Paper} textAlign="center">
      <Typography display="block" variant="caption" color="textSecondary" gutterBottom>
        Upload a file to get started
      </Typography>
      <label htmlFor="add-image-input">
        <Button component="span" size="small" variant="contained" color="primary">
          upload
        </Button>
      </label>
      <input
        accept="image/*"
        style={{ display: "none" }}
        id="add-image-input"
        onChange={handleImageUpload}
        type="file"
      />
    </Box>
  );
};

export default InitialImageUpload;
