import React from "react";
import useStyles from "./styles";
import { clsx } from "utils";
import { Tooltip, ButtonBase } from "@material-ui/core";

type Props = {
  name: string;
  isSelected: boolean;
  icon: React.FunctionComponent<React.SVGProps<SVGSVGElement>>;
  onClick: () => void;
};

const ToolbarItem: React.FC<Props> = ({ isSelected, name, icon: Icon, onClick }) => {
  const classes = useStyles();
  return (
    <Tooltip classes={{ tooltip: classes.tooltip }} title={name} placement="right">
      <ButtonBase
        onClick={onClick}
        disableRipple={isSelected}
        className={clsx(classes.btn, isSelected ? classes.selected : "")}
        aria-label={name}
        aria-selected={isSelected}
      >
        <Icon />
      </ButtonBase>
    </Tooltip>
  );
};

export default ToolbarItem;
