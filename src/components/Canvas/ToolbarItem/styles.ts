import { makeStyles } from "@material-ui/styles";
import { Theme } from "@material-ui/core";
import { fade as fadeColor } from "@material-ui/core/styles/colorManipulator";

export default makeStyles<Theme>(theme => {
  return {
    tooltip: {
      boxShadow: theme.shadows[4],
      background: theme.palette.background.paper
    },
    btn: {
      padding: theme.spacing(0.5),
      borderRadius: 4,
      "&:hover, &:focus": {
        backgroundColor: fadeColor(theme.palette.grey[900], 0.5)
      },
      "&$selected": {
        backgroundColor: theme.palette.grey[900],
        cursor: "initial"
      },
      "& svg, & svg path": {
        height: 20,
        width: 20,
        fill: theme.palette.common.white
      }
    },
    selected: {}
  };
});
