import React from "react";
import { Label, Tag, Text } from "react-konva";
import Konva from "konva";
import { grey } from "@material-ui/core/colors";

type Props = Konva.NodeConfig & {
  isSelected?: boolean;
  label: string;
  fontSize: number;
};

type Ref = Konva.Label;

const Annotation: React.FC<Konva.NodeConfig> = React.forwardRef<Ref, Props>(
  ({ isSelected = false, label, fontSize, ...props }, ref) => {
    const textRef = React.useRef<Konva.Text | null>(null);

    return (
      <Label ref={ref} {...props}>
        <Tag
          shadowColor="#000"
          shadowBlur={16}
          shadowOpacity={isSelected ? 0.9 : 0.5}
          cornerRadius={2}
          fill={grey[800]}
        />
        <Text
          ref={(ref: Konva.Text) => {
            textRef.current = ref;
          }}
          padding={8}
          fill="#FFF"
          fontSize={fontSize}
          text={label}
        />
      </Label>
    );
  }
);

export default Annotation;
