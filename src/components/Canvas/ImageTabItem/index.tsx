import React from "react";
import { ButtonBase } from "@material-ui/core";
import ClearRoundedIcon from "@material-ui/icons/ClearRounded";
import useStyles from "./styles";

type Props = {
  isCurrent: boolean;
  onClick: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  onDelete: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  src: string;
};

const ImageTabItem: React.FC<Props> = ({ isCurrent, onClick, onDelete, src }) => {
  const classes = useStyles();
  return (
    <div className={classes.root} data-testid="image-tab">
      <ButtonBase onClick={onDelete} aria-label="remove image" className={classes.delete}>
        <ClearRoundedIcon />
      </ButtonBase>
      <ButtonBase
        aria-label="image tab"
        disabled={isCurrent}
        onClick={onClick}
        className={classes.img}
      >
        <img src={src} alt="" />
      </ButtonBase>
    </div>
  );
};

export default ImageTabItem;
