import { makeStyles } from "@material-ui/styles";
import { Theme } from "@material-ui/core";

export default makeStyles((theme: Theme) => ({
  root: {
    position: "relative",
    marginBottom: theme.spacing(1),
    "&:hover, &:focus": {
      "& $delete": {
        visibility: "visible"
      }
    }
  },
  tabItem: {
    width: "100%",
    height: 80,
    borderRadius: 2,
    marginBottom: theme.spacing(1)
  },
  img: {
    background: theme.palette.grey[900],
    padding: theme.spacing(0.25),
    objectFit: "contain",
    borderRadius: 2,
    "& img": {
      maxWidth: "100%"
    },
    "&:not(:disabled)": {
      opacity: 0.6,
      "&:focus, &:hover": {
        background: theme.palette.grey[700]
      }
    }
  },
  delete: {
    visibility: "hidden",
    background: theme.palette.grey[600],
    borderRadius: "50%",
    position: "absolute",
    right: 0,
    top: 0,
    transform: "translate(30%, -30%)",
    zIndex: 2,
    "&:focus, &:hover": {
      visibility: "visible"
    },
    "& svg": {
      fill: theme.palette.common.white,
      height: 14,
      width: 14
    }
  }
}));
