import { makeStyles } from "@material-ui/styles";
import { Theme } from "@material-ui/core";

export default makeStyles((theme: Theme) => ({
  root: {
    borderRadius: 2
  },
  list: {
    minWidth: 180
  },
  item: {
    color: theme.palette.text.secondary
  },
  icon: {
    minWidth: 30,
    "& svg": {
      fill: theme.palette.text.secondary,
      height: 16,
      width: 16
    }
  }
}));
