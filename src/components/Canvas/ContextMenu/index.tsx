import React from "react";
import Portal from "components/Canvas/Portal";
import UndoIcon from "@material-ui/icons/Undo";
import RedoIcon from "@material-ui/icons/Redo";
import ShowIcon from "@material-ui/icons/Visibility";
import HideIcon from "@material-ui/icons/VisibilityOff";
import CameraIcon from "@material-ui/icons/CameraAltRounded";
import { Paper, MenuList } from "@material-ui/core";
import useStyles from "./styles";
import ThemeProvider from "components/ThemeProvider";
import ContextMenuItem from "./MenuItem";
import { useCanvasRootCTX } from "contexts/CanvasRootContext";
import Konva from "konva";
import useKeyboardUndoRedo from "hooks/canvas/useKeyboardUndoRedo";
import { useScreenshotsCTX } from "contexts/ScreenshotsContext";

const portalTarget = document.getElementById("portals-root") as HTMLElement;

type Props = {
  pos: Konva.Vector2d;
  show: boolean;
  onClose: () => void;
};

const ContextMenu: React.FC<Props> = ({ pos, show, onClose }) => {
  useKeyboardUndoRedo();
  const classes = useStyles();
  const { dispatch: ssDispatch } = useScreenshotsCTX();
  const { dispatch: rootDispatch, state: rootState } = useCanvasRootCTX();

  const stageItem = rootState.current ? rootState.images[rootState.current] : null;

  const hideAllDisabled = React.useMemo(() => {
    if (!stageItem) return true;

    return stageItem.shapes.every(shape => !shape.show);
  }, [stageItem]);

  const showAllDisabled = React.useMemo(() => {
    if (!stageItem) return true;

    return stageItem.shapes.every(shape => shape.show);
  }, [stageItem]);

  const undoDisabled =
    !stageItem || !stageItem.history.items.length || stageItem.history.current === 0;
  const redoDisabled =
    !stageItem ||
    !stageItem.history.items.length ||
    stageItem.history.items.length === stageItem.history.current + 1;

  const ref = React.useRef<HTMLButtonElement>(null);

  React.useEffect(() => {
    const hideContextMenu = (event): void => {
      if (!show) return;
      // we don't hide the menu if it's the one being clicked
      if (ref.current?.contains(event.target)) return;

      onClose();
    };

    window.addEventListener("mousedown", hideContextMenu);
    return (): void => {
      window.addEventListener("mousedown", hideContextMenu);
    };
  }, [onClose, show]);

  // boilerplate to not put everything that is repeated
  const createContextMenuClick = (callback: (event) => void) => (event): void => {
    event.stopPropagation();
    onClose();
    callback(event);
  };

  const handleRedo = createContextMenuClick(() => {
    rootDispatch({ type: "redo" });
  });

  const handleUndo = createContextMenuClick(() => {
    rootDispatch({ type: "undo" });
  });

  const handleShowAll = createContextMenuClick(() => {
    rootDispatch({ type: "toggle_shapes_visibility", payload: true });
  });
  const handleHideAll = createContextMenuClick(() => {
    rootDispatch({ type: "toggle_shapes_visibility", payload: false });
  });

  const handleScreenshot = createContextMenuClick(() => {
    if (!rootState.stage) return;

    const image = rootState.stage.toDataURL();
    ssDispatch({ type: "open_add_modal", payload: image });
  });

  return (
    <Portal node={portalTarget}>
      <ThemeProvider>
        <Paper
          ref={ref}
          aria-hidden={!show}
          style={{
            top: pos.y,
            left: pos.x,
            position: "absolute",
            display: show ? "initial" : "none"
          }}
          className={classes.root}
          elevation={6}
          square
        >
          <MenuList className={classes.list}>
            <ContextMenuItem
              disabled={undoDisabled}
              icon={UndoIcon}
              label="Undo"
              onClick={handleUndo}
            />
            <ContextMenuItem
              disabled={redoDisabled}
              icon={RedoIcon}
              label="Redo"
              onClick={handleRedo}
            />
            <ContextMenuItem
              disabled={showAllDisabled}
              icon={ShowIcon}
              label="Show all"
              onClick={handleShowAll}
            />
            <ContextMenuItem
              disabled={hideAllDisabled}
              icon={HideIcon}
              label="Hide all"
              onClick={handleHideAll}
            />
            <ContextMenuItem
              disabled={!rootState.stage}
              icon={CameraIcon}
              label="Capture"
              onClick={handleScreenshot}
            />
          </MenuList>
        </Paper>
      </ThemeProvider>
    </Portal>
  );
};

export default React.memo(ContextMenu);
