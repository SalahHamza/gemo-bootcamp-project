import React from "react";
import { MenuItem, ListItemIcon, Typography, IconProps } from "@material-ui/core";
import useStyles from "./styles";

type MenuItemType = {
  label: string;
  icon: React.ElementType<IconProps>;
  onClick: (event) => void;
  disabled?: boolean;
};

const ContextMenuItem: React.FC<MenuItemType> = ({
  label,
  icon: Icon,
  onClick,
  disabled = false
}) => {
  const classes = useStyles();

  return (
    <MenuItem disabled={disabled} onClick={onClick}>
      <ListItemIcon className={classes.icon}>
        <Icon fontSize="small" />
      </ListItemIcon>
      <Typography variant="caption" color="inherit">
        {label}
      </Typography>
    </MenuItem>
  );
};

export default ContextMenuItem;
