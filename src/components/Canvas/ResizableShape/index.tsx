import React from "react";
import { ShapeItem, ShapeCircleConf, ShapeRectConf } from "constants/canvas";
import { getRgbaString } from "utils/canvas";
import { useCanvasRootBridge } from "contexts/CanvasRootContext";
import { useCanvasConfBridge } from "contexts/CanvasConfContext";
import ResizableRect from "./ResizableRect";
import ResizableCircle from "./ResizableCircle";
import Konva from "konva";

type Props = {
  shape: ShapeItem;
};

const ResizableShape: React.FC<Props> = ({ shape }) => {
  const { state: rootState, dispatch: rootDispatch } = useCanvasRootBridge();
  const { state: confState } = useCanvasConfBridge();
  // since we are inside resizable shape, stageItem exists
  const stageItem = rootState.images[rootState.current as string];

  const isSelected = stageItem.selected === shape.id;

  const onShapeClick = (event): void => {
    // we stop the bubbling of the click event
    // check here for more details:
    // https://konvajs.org/docs/events/Cancel_Propagation.html
    // https://github.com/konvajs/react-konva/issues/139
    event.cancelBubble = true;
  };

  const onShapeMouseDown = (event): void => {
    if (confState.cursor !== "pointer") return;

    // we only cancel bubble if not on grab mode
    // i.e. not on screen dragging
    if (!confState.canGrab) {
      // when the mouseDown event bubbles up to the stage
      // we create a new shape while dragging the current one
      // that's why we stop the bubbling of the mouse event
      event.cancelBubble = true;
    }

    // we check first if the clicked shape is not
    // aleady selected
    if (!isSelected && !confState.canGrab) {
      // we select the clicked shape
      rootDispatch({ type: "set_selected", payload: shape.id });
    }
  };

  // We return a function so that we can
  // get a closure that contains the shape shape
  const onShapeDragEnd = (event): void => {
    // we are stopping the bubbling here, because
    // it triggers stage drag event => changes stageX/Y
    event.evt.preventDefault();
    event.cancelBubble = true;
    const rect = {
      ...shape,
      x: event.target.x(),
      y: event.target.y()
    };
    rootDispatch({ type: "update_shape", payload: rect });
  };

  const onShapeResizeEnd = (shapeConf: ShapeCircleConf | ShapeRectConf): void => {
    const newShape = {
      ...shape,
      ...shapeConf
    };
    rootDispatch({ type: "update_shape", payload: newShape });
  };

  const onAnnotationPosChange = React.useCallback(
    event => {
      const annotNode = event.target as Konva.Label;
      if (!annotNode) return;

      // we are stopping the bubbling here, because
      // it triggers stage drag event => changes stageX/Y
      event.evt.preventDefault();
      event.cancelBubble = true;

      rootDispatch({
        type: "update_annotation_position",
        payload: {
          pos: annotNode.getPosition(),
          shapeId: shape.id
        }
      });
    },
    [rootDispatch, shape.id]
  );

  // NOTE: typescript doesn't check spread'ed props
  const sharedProps = {
    x: shape.x,
    y: shape.y,
    isSelected: isSelected,
    fontSize: 15 / (stageItem.scale || 1),
    fill: getRgbaString(shape.color, true),
    stroke: getRgbaString(shape.color),
    label: shape.annotation,
    // we only allow dragging of shapes if the cursor
    // is a pointer.
    draggable: confState.cursor === "pointer" && !confState.canGrab,
    // event handlers
    onClick: onShapeClick,
    onDragEnd: onShapeDragEnd,
    onMouseDown: onShapeMouseDown,
    onResizeEnd: onShapeResizeEnd,
    onAnnotationPosChange: onAnnotationPosChange,
    annotationPos: shape.annotationPos
  };

  switch (shape.type) {
    case "rect":
      return <ResizableRect width={shape.width} height={shape.height} {...sharedProps} />;
    case "circle":
      return <ResizableCircle radius={Math.abs(shape.radius)} {...sharedProps} />;
    default:
      return null;
  }
};

export default ResizableShape;
