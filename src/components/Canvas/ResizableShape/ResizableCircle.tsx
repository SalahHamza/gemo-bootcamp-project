import React from "react";
import { Arrow, Transformer } from "react-konva";
import { getClosestPointToRect, getClosestPointToCircle } from "utils/arrow";
import Konva from "konva";
import Annotation from "../Annotation";
import { ShapeCircleConf, MIN_Shape_SIZE } from "constants/canvas";
import { CanvasCircle } from "../Shapes";
import { isOutsideNode } from "utils/canvas";

/**
 * @todo remove this when it's exposed from konva
 * https://github.com/konvajs/konva/issues/705
 */
interface Box {
  rotation: number;
  x: number;
  y: number;
  width: number;
  height: number;
}

type Props = Konva.CircleConfig & {
  isSelected: boolean;
  onResizeEnd: (shape: ShapeCircleConf) => void;
  fontSize: number;
  onAnnotationPosChange: (pos: Konva.Vector2d) => void;
  annotationPos: Konva.Vector2d | null;
};

const ResizableCircle: React.FC<Props> = ({
  label,
  onMouseDown,
  draggable,
  isSelected,
  onAnnotationPosChange,
  annotationPos,
  onResizeEnd,
  fontSize,
  ...props
}) => {
  const annotRef = React.useRef<Konva.Label | null>(null);
  const shapeRef = React.useRef<Konva.Circle | null>(null);
  const transRef = React.useRef<Konva.Transformer | null>(null);

  const [linePoints, setLinePoints] = React.useState();

  React.useEffect(() => {
    const transformer = transRef.current as Konva.Transformer;
    if (isSelected && transformer) {
      // we need to attach transformer manually
      transformer.setNode(shapeRef.current);
      transformer.getLayer()?.batchDraw();
    }
  }, [isSelected]);

  const updateLinePoints = React.useCallback(() => {
    const annotNode = annotRef.current as Konva.Label;
    const shapeNode = shapeRef.current as Konva.Circle;
    if (!label || !annotNode || !shapeNode) return;

    const fromPos = annotNode.getPosition();
    const toPos = shapeNode.getPosition();

    const fromWidth = annotNode.width();
    const fromHeight = annotNode.height();
    const radius = shapeNode.radius();

    const arrowStart = getClosestPointToRect(
      {
        height: fromHeight,
        width: fromWidth,
        ...fromPos
      },
      toPos
    );
    const arrowEnd = getClosestPointToCircle(
      {
        radius,
        ...toPos
      },
      fromPos
    );

    setLinePoints([arrowStart.x, arrowStart.y, arrowEnd.x, arrowEnd.y]);
  }, [label]);

  React.useEffect(() => {
    const annotNode = annotRef.current as Konva.Label;
    if (!annotNode) return;

    const fromPos = annotationPos ?? annotNode.getPosition();
    const toPos = { x: props.x as number, y: props.y as number };
    const radius = props.radius;

    const fromWidth = annotNode.width();
    const fromHeight = annotNode.height();

    const arrowStart = getClosestPointToRect(
      {
        height: fromHeight,
        width: fromWidth,
        ...fromPos
      },
      toPos
    );
    const arrowEnd = getClosestPointToCircle(
      {
        radius,
        ...toPos
      },
      fromPos
    );

    setLinePoints([arrowStart.x, arrowStart.y, arrowEnd.x, arrowEnd.y]);
  }, [annotationPos, props.radius, props.x, props.y]);

  const handleAnnotationDragMove = () => {
    updateLinePoints();
  };

  const handleCircleDragMove = () => {
    updateLinePoints();
  };

  const handleTransformEnd = (): void => {
    // transformer is changing scale of the node
    // and NOT its width or height
    // but in the store we have only width and height
    // to match the data better we will reset scale on transform end
    const node = shapeRef.current as Konva.Circle;
    const scaleX = node.scaleX();

    // we will reset it back
    node.scale({ x: 1, y: 1 });
    onResizeEnd({
      type: "circle",
      x: node.x(),
      y: node.y(),
      // set minimal value
      radius: Math.max(MIN_Shape_SIZE / 2, node.radius() * scaleX)
    });
    updateLinePoints();
  };

  const cancelBubbleUp = (event): void => {
    event.evt.preventDefault();
    event.cancelBubble = true;
  };

  // This function does too many things
  // and it is called whenever a shape is dragged
  // NOTE: this needs to be refactored
  const dragBoundFunc = React.useCallback(
    (pos: Konva.Vector2d): Konva.Vector2d => {
      const stage = shapeRef.current?.getStage() as Konva.Stage;
      const image = stage.findOne("Image") as Konva.Image;

      if (!image) return pos;

      // this is the correct scale of the image
      // .getAbsoluteScale()  takes into account
      // node's ancestor scales so this is the
      // same as the scale in the state
      // and the same as stage.scale()
      // this is here only for doc purposes
      const scale = image.getAbsoluteScale().x;

      // we scale circle radius
      const radius = props.radius * scale;

      const offsetX = image.offsetX() * scale;
      const offsetY = image.offsetY() * scale;

      // height/width are not scaled by default
      // so we need to scale them appropriately
      const imageH = image.height() * scale;
      const imageW = image.width() * scale;

      // absolute position is scaled by default
      const absPos = image.getAbsolutePosition();
      const imageX = absPos.x - offsetX;
      const imageY = absPos.y - offsetY;

      const x =
        pos.x - radius < imageX
          ? imageX + radius
          : pos.x + radius > imageX + imageW
          ? imageX + imageW - radius
          : pos.x;
      const y =
        pos.y - radius < imageY
          ? imageY + radius
          : pos.y + radius > imageY + imageH
          ? imageY + imageH - radius
          : pos.y;

      return { x, y };
    },
    [props.radius]
  );

  function boundBoxFunc(oldBox: Box, newBox: Box): Box {
    const stage = shapeRef.current?.getStage() as Konva.Stage;
    const image = stage.findOne("Image");

    // limit resize
    if (newBox.width < MIN_Shape_SIZE / 2 || isOutsideNode(image)) {
      return oldBox;
    }
    return newBox;
  }

  return (
    <React.Fragment>
      <CanvasCircle
        {...props}
        ref={shapeRef}
        onContextMenu={(e): void => e.evt.preventDefault()}
        strokeWidth={4}
        onDragMove={handleCircleDragMove}
        onTransformEnd={handleTransformEnd}
        draggable={draggable}
        onMouseDown={onMouseDown}
        shadowColor="#000"
        shadowBlur={16}
        shadowOpacity={isSelected ? 0.9 : 0.5}
        shadowEnabled
        dragBoundFunc={dragBoundFunc}
      />
      {isSelected && (
        <Transformer
          ref={transRef}
          anchorSize={4}
          anchorFill={props.stroke}
          onClick={cancelBubbleUp}
          onMouseDown={cancelBubbleUp}
          boundBoxFunc={boundBoxFunc}
          rotateEnabled={false}
          keepRatio={true}
          borderEnabled={false}
        />
      )}

      <Annotation
        // we can't conditionally show/hide annotation since
        // we want its ref.
        visible={!!label}
        x={annotationPos?.x}
        y={annotationPos?.y}
        onContextMenu={(e): void => e.evt.preventDefault()}
        onClick={cancelBubbleUp}
        onMouseDown={onMouseDown}
        fontSize={fontSize}
        label={label}
        ref={annotRef}
        onDragEnd={onAnnotationPosChange}
        onDragMove={handleAnnotationDragMove}
        draggable={draggable}
      />

      {!!label && linePoints ? (
        <Arrow
          points={linePoints}
          stroke="#FF0"
          fill="#FF0"
          strokeWidth={2}
          lineJoin="round"
          /* line with 5px gap and 10px line segments */
          dash={[10, 5]}
        />
      ) : null}
    </React.Fragment>
  );
};

export default ResizableCircle;
