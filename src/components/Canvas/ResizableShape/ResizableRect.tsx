import React from "react";
import Konva from "konva";
import { Arrow, Transformer } from "react-konva";
import Annotation from "components/Canvas/Annotation";
import { ShapeRectConf } from "constants/canvas";
import { getClosestPointToRect } from "utils/arrow";
import { CanvasRect } from "components/Canvas/Shapes";
import { isOutsideNode } from "utils/canvas";

type Props = Konva.RectConfig & {
  isSelected: boolean;
  onResizeEnd: (rect: ShapeRectConf) => void;
  onAnnotationPosChange: (pos: Konva.Vector2d) => void;
  annotationPos: Konva.Vector2d | null;
  fontSize;
};

/**
 * adapted from:
 * https://konvajs.org/docs/react/Transformer.html
 *
 * @todo Check Box if Box type exists, if not try
 * to make a PR to make accessible.
 */
const ResizableRect: React.FC<Props> = ({
  label,
  onMouseDown,
  onAnnotationPosChange,
  annotationPos,
  draggable,
  isSelected,
  onResizeEnd,
  fontSize,
  ...props
}) => {
  const labelRef = React.useRef<Konva.Label | null>(null);
  const shapeRef = React.useRef<Konva.Rect | null>(null);
  const transRef = React.useRef<Konva.Transformer | null>(null);

  // line points state for the annotation arrow
  const [linePoints, setLinePoints] = React.useState<number[] | null>(null);

  React.useEffect(() => {
    const transformer = transRef.current as Konva.Transformer;
    if (isSelected && transformer) {
      // we need to attach transformer manually
      transformer.setNode(shapeRef.current);
      transformer.getLayer()?.batchDraw();
    }
  }, [isSelected]);

  const updateLinePoints = React.useCallback(() => {
    const annotNode = labelRef.current as Konva.Label;
    const shapeNode = shapeRef.current as Konva.Rect;
    // checking for the annotNode is enough, but we need
    // to update lines whenever the label changes, so might
    // as well check for it too.
    if (!label || !shapeNode || !annotNode) return;
    const fromPos = annotNode.getPosition();
    const toPos = shapeNode.getPosition();

    const fromWidth = annotNode.width();
    const fromHeight = annotNode.height();
    const toWidth = shapeNode.width();
    const toHeight = shapeNode.height();

    const toX = toPos.x;
    const toY = toPos.y;

    const arrowStart = getClosestPointToRect(
      {
        height: fromHeight,
        width: fromWidth,
        ...fromPos
      },
      toPos
    );
    const arrowEnd = getClosestPointToRect(
      {
        height: toHeight,
        width: toWidth,
        x: toX,
        y: toY
      },
      fromPos
    );
    setLinePoints([arrowStart.x, arrowStart.y, arrowEnd.x, arrowEnd.y]);
  }, [label]);

  React.useEffect(() => {
    const annotNode = labelRef.current as Konva.Label;
    // checking for the annotNode is enough, but we need
    // to update lines whenever the label changes, so might
    // as well check for it too.
    if (!annotNode) return;
    const fromPos = annotationPos ?? annotNode.getPosition();
    const toPos = { x: props.x as number, y: props.y as number };

    const fromWidth = annotNode.width();
    const fromHeight = annotNode.height();
    const toWidth = props.width as number;
    const toHeight = props.height as number;

    const toX = toPos.x;
    const toY = toPos.y;

    const arrowStart = getClosestPointToRect(
      {
        height: fromHeight,
        width: fromWidth,
        ...fromPos
      },
      toPos
    );
    const arrowEnd = getClosestPointToRect(
      {
        height: toHeight,
        width: toWidth,
        x: toX,
        y: toY
      },
      fromPos
    );
    setLinePoints([arrowStart.x, arrowStart.y, arrowEnd.x, arrowEnd.y]);
  }, [annotationPos, props.height, props.width, props.x, props.y]);

  const handleAnnotationDragMove = () => {
    updateLinePoints();
  };

  const handleRectDragMove = () => {
    updateLinePoints();
  };

  const handleTransformEnd = () => {
    // transformer is changing scale of the node
    // and NOT its width or height
    // but in the store we have only width and height
    // to match the data better we will reset scale on transform end
    const node = shapeRef.current as Konva.Rect;
    const scaleX = node.scaleX();
    const scaleY = node.scaleY();

    // we will reset it back
    node.scaleX(1);
    node.scaleY(1);
    onResizeEnd({
      type: "rect",
      x: node.x(),
      y: node.y(),
      // set minimal value
      width: Math.max(5, node.width() * scaleX),
      height: Math.max(node.height() * scaleY)
    });

    updateLinePoints();
  };

  const cancelBubbleUp = event => {
    event.evt.preventDefault();
    event.cancelBubble = true;
  };

  // This function does too many things
  // and it is called whenever a shape is dragged
  const rectDragBoundFn = React.useCallback(
    (pos: Konva.Vector2d): Konva.Vector2d => {
      const stage = shapeRef.current?.getStage() as Konva.Stage;
      const image = stage.findOne("Image") as Konva.Image;

      if (!image) return pos;

      // this is the correct scale of the image
      // .getAbsoluteScale()  takes into account
      // node's ancestor scales so this is the
      // same as the scale in the state
      // and the same as stage.scale()
      // this is here only for doc purposes
      const scale = image.getAbsoluteScale().x;

      // we scale the shape's size to fit the scale
      const shapeH = (props.height as number) * scale;
      const shapeW = (props.width as number) * scale;

      // height/width are not scaled by default
      // so we need to scale them appropriately
      const offsetX = image.offsetX() * scale;
      const offsetY = image.offsetY() * scale;

      const imageH = image.height() * scale;
      const imageW = image.width() * scale;

      // absolute position is scaled by default
      const imageX = image.getAbsolutePosition().x - offsetX;
      const imageY = image.getAbsolutePosition().y - offsetY;

      const x =
        pos.x < imageX
          ? imageX
          : pos.x + shapeW > imageX + imageW
          ? imageX + imageW - shapeW
          : pos.x;
      const y =
        pos.y < imageY
          ? imageY
          : pos.y + shapeH > imageY + imageW
          ? imageY + imageH - shapeH
          : pos.y;

      return { x, y };
    },
    [props.height, props.width]
  );

  function boundBoxFunc<B>(oldBox, newBox): B {
    const stage = shapeRef.current?.getStage() as Konva.Stage;
    const image = stage.findOne("Image");
    // limit resize
    if (newBox.width < 30 || newBox.height < 30 || isOutsideNode(image)) {
      return oldBox;
    }
    return newBox;
  }

  return (
    <React.Fragment>
      <CanvasRect
        {...props}
        ref={shapeRef}
        onDragMove={handleRectDragMove}
        onTransformEnd={handleTransformEnd}
        draggable={draggable}
        onMouseDown={onMouseDown}
        onContextMenu={(e): void => e.evt.preventDefault()}
        strokeWidth={4}
        shadowColor="#000"
        shadowBlur={16}
        shadowOpacity={isSelected ? 0.9 : 0.5}
        shadowEnabled
        dragBoundFunc={rectDragBoundFn}
      />
      {isSelected && (
        <Transformer
          ref={transRef}
          anchorSize={4}
          anchorFill={props.stroke}
          onClick={cancelBubbleUp}
          onMouseDown={cancelBubbleUp}
          rotateEnabled={false}
          keepRatio={false}
          borderEnabled={false}
          boundBoxFunc={boundBoxFunc}
        />
      )}

      <Annotation
        // we can't conditionally show/hide annotation since
        // we want its ref.
        visible={!!label}
        x={annotationPos?.x}
        y={annotationPos?.y}
        onContextMenu={(e): void => e.evt.preventDefault()}
        onClick={cancelBubbleUp}
        onMouseDown={onMouseDown}
        fontSize={fontSize}
        label={label}
        ref={labelRef}
        onDragEnd={onAnnotationPosChange}
        onDragMove={handleAnnotationDragMove}
        draggable={draggable}
      />

      {!!label && linePoints ? (
        <Arrow
          points={linePoints}
          stroke="#FF0"
          fill="#FF0"
          strokeWidth={2}
          lineJoin="round"
          /* line with 5px gap and 10px line segments */
          dash={[10, 5]}
        />
      ) : null}
    </React.Fragment>
  );
};

export default ResizableRect;
