import React from "react";
import { Slider, ButtonBase } from "@material-ui/core";
import ZoomInIcon from "@material-ui/icons/ZoomIn";
import ZoomOutIcon from "@material-ui/icons/ZoomOut";
import useStyles from "./styles";
import {
  CANVAS_ZOOM_STEP,
  CANVAS_MAX_SCALE,
  CANVAS_MIN_SCALE,
  CANVAS_DEFAULT_SCALE
} from "constants/canvas";
import { useCanvasRootCTX } from "contexts/CanvasRootContext";
import { roundToNPlace } from "utils";

const ZoomSlider: React.FC = () => {
  const classes = useStyles();

  const { dispatch: rootDispatch, state: rootState } = useCanvasRootCTX();

  const stageItem = rootState.current ? rootState.images[rootState.current] : null;

  const handleZoomInClick = () => {
    if (!stageItem) return;
    const newScale = Math.min(stageItem.scale + CANVAS_ZOOM_STEP, CANVAS_MAX_SCALE);
    rootDispatch({ type: "set_stage_scale", payload: roundToNPlace(newScale, 3) });
  };

  const handleZoomOutClick = () => {
    if (!stageItem) return;
    const newScale = Math.max(stageItem.scale - CANVAS_ZOOM_STEP, CANVAS_MIN_SCALE);
    rootDispatch({ type: "set_stage_scale", payload: roundToNPlace(newScale, 3) });
  };

  const handleSliderChange = (event, newValue) => {
    if (!stageItem) return;
    rootDispatch({ type: "set_stage_scale", payload: roundToNPlace(newValue, 3) });
  };

  const isDisabled = typeof stageItem?.scale !== "number";

  return (
    <div className={classes.root}>
      <ButtonBase
        onClick={handleZoomOutClick}
        disabled={isDisabled || stageItem?.scale === CANVAS_MIN_SCALE}
        aria-label="zoom out"
        className={classes.btn}
      >
        <ZoomOutIcon />
      </ButtonBase>
      <div className={classes.sliderWrapper}>
        <Slider
          disabled={isDisabled}
          className={classes.slider}
          step={CANVAS_ZOOM_STEP}
          value={stageItem?.scale || CANVAS_DEFAULT_SCALE}
          onChange={handleSliderChange}
          min={CANVAS_MIN_SCALE}
          max={CANVAS_MAX_SCALE}
          aria-label="zoom slider"
        />
      </div>
      <ButtonBase
        onClick={handleZoomInClick}
        disabled={isDisabled || stageItem?.scale === CANVAS_MAX_SCALE}
        aria-label="zoom in"
        className={classes.btn}
      >
        <ZoomInIcon />
      </ButtonBase>
    </div>
  );
};

export default ZoomSlider;
