import { makeStyles, Theme } from "@material-ui/core";

export default makeStyles((theme: Theme) => ({
  root: {
    position: "absolute",
    bottom: theme.spacing(2),
    left: "50%",
    transform: "translateX(-50%)",
    width: 210,
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    borderRadius: 50,
    boxShadow: theme.shadows[5],
    background: theme.palette.background.paper,
    overflow: "hidden",
    "& svg": {
      fill: "white"
    }
  },
  btn: {
    padding: theme.spacing(0.5)
  },
  sliderWrapper: {
    padding: theme.spacing(0, 1),
    lineHeight: 1,
    flexGrow: 1
  },
  slider: {
    float: "left"
  }
}));
