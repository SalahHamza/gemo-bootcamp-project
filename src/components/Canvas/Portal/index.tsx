import React from "react";
import ReactDOM from "react-dom";

type Props = {
  // node to inject portal children to
  node?: HTMLElement;
};

// adapted from https://konvajs.org/docs/react/DOM_Portal.html

class Portal extends React.Component<Props> {
  defaultNode: HTMLElement | null = null;

  componentDidMount(): void {
    this.renderPortal();
  }

  componentDidUpdate(): void {
    this.renderPortal();
  }

  componentWillUnmount(): void {
    ReactDOM.unmountComponentAtNode((this.defaultNode as HTMLElement) || this.props.node);
    if (!this.defaultNode) return;
    // we first check if the node is descendant of document.body
    if (!document.body.contains(this.props.node || this.defaultNode)) return;
    document.body.removeChild(this.props.node || this.defaultNode);
  }

  renderPortal(): void {
    if (!this.props.node || !this.defaultNode) {
      this.defaultNode = document.createElement("div");
      document.body.appendChild(this.defaultNode);
    }

    let children = this.props.children as React.ReactElement;
    if (children) {
      if (typeof children.type === "function") {
        children = React.cloneElement(children);
      }

      ReactDOM.render(children, this.props.node || this.defaultNode);
    }
  }

  render(): null {
    return null;
  }
}

export default Portal;
