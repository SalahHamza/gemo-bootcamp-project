import React from "react";
import { Paper } from "@material-ui/core";
import { ReactComponent as CursorPointerIcon } from "assets/svg/cursor.svg";
import { ReactComponent as RectIcon } from "assets/svg/rect.svg";
import { ReactComponent as CircleIcon } from "assets/svg/circle.svg";
import useStyles from "./styles";
import ToolbarItem from "components/Canvas/ToolbarItem";
import { CanvasToolItemType } from "constants/canvas";
import { useCanvasConfCTX } from "contexts/CanvasConfContext";

type ToolType = {
  key: CanvasToolItemType;
  name: string;
  icon: React.FunctionComponent<React.SVGProps<SVGSVGElement>>;
};

const tools: ToolType[] = [
  {
    key: "pointer",
    name: "Pointer",
    icon: CursorPointerIcon
  },
  {
    key: "drawer:rect",
    name: "rectangle",
    icon: RectIcon
  },
  {
    key: "drawer:circle",
    name: "circle",
    icon: CircleIcon
  }
];

const Toolbar: React.FC = () => {
  const classes = useStyles();
  const {
    dispatch,
    state: { cursor }
  } = useCanvasConfCTX();

  const handleClick = (key: CanvasToolItemType) => (): void => {
    // there is no need to trigger a change the value
    // is the same.
    // Note: disabling the button itself is not an option
    // since the tooltip uses the hover event to show itself.
    if (cursor === key) return;
    dispatch({ type: "set_cursor", payload: key });
  };

  return (
    <Paper role="toolbar" className={classes.root} elevation={6} square>
      {tools.map(({ key, ...rest }) => (
        <ToolbarItem onClick={handleClick(key)} key={key} {...rest} isSelected={key === cursor} />
      ))}
    </Paper>
  );
};

export default Toolbar;
