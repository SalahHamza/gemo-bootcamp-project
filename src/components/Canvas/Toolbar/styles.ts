import { makeStyles } from "@material-ui/styles";
import { Theme } from "@material-ui/core";

export default makeStyles<Theme>(theme => {
  return {
    root: {
      padding: theme.spacing(0.75),
      position: "absolute",
      top: theme.spacing(2),
      left: theme.spacing(1),
      borderRadius: 2,
      display: "grid",
      gap: `${theme.spacing(0.5)}px`,
      gridAutoFlow: "row"
    }
  };
});
