import React from "react";
import useStyles from "./styles";
import useGetContainerSize from "hooks/canvas/useGetContainerSize";
import CanvasStage from "components/Canvas/CanvasStage";
import CanvasZoomSlider from "components/Canvas/ZoomSlider";
import Toolbar from "components/Canvas/Toolbar";
import ImageTabs from "components/Canvas/ImageTabs";
import { useCanvasConfCTX } from "contexts/CanvasConfContext";
import useCanvasSpaceGrabbing from "hooks/canvas/useCanvasSpaceGrabbing";
import ContextMenu from "./ContextMenu";
import useGetCanvasImage from "hooks/canvas/useGetCanvasImageEl";
import { useCanvasRootCTX } from "contexts/CanvasRootContext";
import InitialImageUpload from "./InitialImageUpload";

const CanvasRoot: React.FC = () => {
  const { state: confState } = useCanvasConfCTX();
  const { state: rootState } = useCanvasRootCTX();
  const classes = useStyles({
    cursor: confState.cursor,
    grabbing: confState.canGrab
  });

  const imageEl = useGetCanvasImage();

  // we invoke the hook that takes care of the canvas
  // space key press grabbing.
  useCanvasSpaceGrabbing();

  const { ref: containerRef, size } = useGetContainerSize();

  const canvas: React.ReactNode = React.useMemo(() => {
    if (!size) return null;
    // we show a message if there are no images yet.
    if (!!rootState.images.current || Object.keys(rootState.images).length === 0) {
      return <InitialImageUpload />;
    }
    return <CanvasStage {...size} imageEl={imageEl} />;
  }, [imageEl, rootState.images, size]);

  const [menuState, setMenuState] = React.useState({ pos: { x: 0, y: 0 }, show: false });

  const openContextMenu = React.useCallback((event): void => {
    event.preventDefault();
    // position of the event within the document
    // we need this to position the menu within the
    // portal that is a direct child of document.body
    const x = event.clientX;
    const y = event.clientY;

    setMenuState({
      pos: { x, y },
      show: true
    });
  }, []);

  const closeContextMenu = React.useCallback(() => {
    setMenuState(prev => ({ ...prev, show: false }));
  }, []);

  return (
    <React.Fragment>
      <div
        id="canvas-stage"
        ref={containerRef}
        onContextMenu={openContextMenu}
        className={classes.canvasStage}
      >
        {canvas}
      </div>

      <Toolbar />
      <CanvasZoomSlider />
      <ImageTabs />
      <ContextMenu pos={menuState.pos} show={menuState.show} onClose={closeContextMenu} />
    </React.Fragment>
  );
};

export default CanvasRoot;
