import React from "react";
import { useCanvasRootBridge } from "contexts/CanvasRootContext";
import ResizableShape from "components/Canvas/ResizableShape";
import { bringForward } from "utils/canvas";

const ShapesGroup: React.FC = () => {
  const { state: rootState } = useCanvasRootBridge();

  // since we are inside resizable shape, stageItem exists
  const stageItem = rootState.current ? rootState.images[rootState.current] : null;

  const orderedRects = React.useMemo(() => {
    if (!stageItem?.shapes) return [];
    return bringForward(stageItem?.shapes, stageItem?.selected);
  }, [stageItem]);

  return (
    <React.Fragment>
      {orderedRects.map(shape =>
        shape.show ? <ResizableShape key={shape.id} shape={shape} /> : null
      )}
    </React.Fragment>
  );
};

export default React.memo(ShapesGroup);
