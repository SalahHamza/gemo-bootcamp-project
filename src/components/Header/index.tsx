import React from "react";
import { AppBar, Toolbar } from "@material-ui/core";
import useStyles from "./styles";

const Header: React.FC = () => {
  const classes = useStyles();
  return (
    <AppBar className={classes.root} position="relative" elevation={0} color="inherit">
      <Toolbar></Toolbar>
    </AppBar>
  );
};

export default Header;
