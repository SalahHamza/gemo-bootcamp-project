import {
  ShapeItem,
  CANVAS_RECT_PREFIX,
  RgbaColor,
  SHAPE_DEFAULT_COLOR_MAP,
  MIN_Shape_SIZE,
  ShapeRectConf,
  ShapeCircleConf
} from "constants/canvas";
import { uniqId } from "utils";
import Konva from "konva";

/**
 * Gets relative pointer position of given node
 * check here for details:
 * https://konvajs.org/docs/sandbox/Relative_Pointer_Position.html
 * @param node
 */
export const getRelativePointerPosition = (node): Konva.Vector2d => {
  // the function will return pointer position relative to the passed node
  const transform = node.getAbsoluteTransform().copy();
  // to detect relative position we need to invert transform
  transform.invert();

  // get pointer (say mouse or touch) position
  const pos = node.getStage().getPointerPosition();

  // now we find relative point
  return transform.point(pos);
};

/**
 * Given a position that is relative to the stage
 * this function will return a position relative
 * to the given node.
 *
 * NOTE: the given pos must be relative to stage (default)
 * and the returned position isn't scaled
 */
export const getPositionRelativeToNode = (
  node: Konva.Node,
  pos: Konva.Vector2d
): Konva.Vector2d => {
  // the function will return pointer position relative to the passed node
  const transform = node.getAbsoluteTransform().copy();
  // to detect relative position we need to invert transform
  transform.invert();

  // now we find relative point
  return transform.point(pos);
};

/**
 * Checks if pointer is outside given canvas node
 * @param node - Node to check if pointer is outside
 * @param offset - value to offset the check up with
 */
export const isOutsideNode = (node: Konva.Node, offset = { x: 0, y: 0 }): boolean => {
  // we get the pointer position to the node (not scaled)
  const relPos = getRelativePointerPosition(node);

  return (
    relPos.x + offset.x < 0 ||
    relPos.y + offset.y < 0 ||
    relPos.x - offset.x > node.width() ||
    relPos.y - offset.y > node.height()
  );
};

export const isValidShapeSize = (shape: ShapeItem): boolean => {
  switch (shape.type) {
    case "rect":
      return Math.abs(shape.width) > MIN_Shape_SIZE && Math.abs(shape.height) > MIN_Shape_SIZE;
    case "circle":
      return Math.abs(shape.radius) > MIN_Shape_SIZE / 2;
    default:
      throw new Error(`unrecognized shape given`);
  }
};

/**
 * adjust rect's conf incase heigth or width are negative.
 */
const adjustShapeConf = (shape: ShapeRectConf | ShapeCircleConf) => {
  switch (shape.type) {
    case "rect": {
      const width = Math.abs(shape.width);
      const height = Math.abs(shape.height);
      const x = shape.width < 0 ? shape.x + shape.width : shape.x;
      const y = shape.height < 0 ? shape.y + shape.height : shape.y;
      return { ...shape, width, height, x, y };
    }
    default:
      return shape;
  }
};

/**
 *
 * creates a boilerplate for the given shape item
 */
export const createNewShape = (shapeConf: ShapeRectConf | ShapeCircleConf): ShapeItem => {
  // sometimes the size of a shape might be negative
  // so we make sure to adjust the conf of that shape
  // before creating a shape object of it.
  const shape = adjustShapeConf(shapeConf);

  return {
    // Note: The order here is important.
    // we are spreading the defaultRectConf and then overriding
    // it with the given RectConf or Point
    ...shape,
    color: SHAPE_DEFAULT_COLOR_MAP,
    annotation: "",
    annotationPos: null,
    show: true,
    id: uniqId()
  };
};

/**
 *
 * this helps reorder the shapes to show the currently
 * selected as the one on top.
 * NOTE: the last shape is the one showing on top.
 * details:
 * https://konvajs.org/docs/react/zIndex.html
 * @param shapes - shape array to re-order
 * @param shapeId - shape id to bring forward
 */
export const bringForward = (shapes: ShapeItem[], shapeId: string | null): ShapeItem[] => {
  if (!shapeId) return shapes;
  const selectedShape = shapes.find(r => r.id === shapeId);
  // technically it should be present, but just in case
  // we return the shapes if the item doesn't exist.
  if (!selectedShape) return shapes;

  return shapes.filter(r => r.id !== shapeId).concat(selectedShape);
};

type RectsSnapsCollection = {
  [key: string]: string;
};

/**
 *
 * extracts an image for each 'CANVAS_RECT_PREFIX" rect
 */
export const getStageRectsSnapshots = (stage): RectsSnapsCollection => {
  const rects = stage.find((node: Konva.Node) => {
    return node.getType() === "Shape" && node.attrs.name?.startsWith(CANVAS_RECT_PREFIX);
  });
  rects.hide();

  return rects.toArray().reduce((acc, node) => {
    const rectId = node.attrs.name?.replace(CANVAS_RECT_PREFIX, "");
    if (!rectId) return acc;

    node.show();
    const image = node.getStage()?.toDataURL();
    node.hide();

    return {
      ...acc,
      [rectId]: image
    };
  }, {});
};

/**
 * Turns given rgba object to a string;
 */
export const getRgbaString = (color: RgbaColor, withOpacity = false): string => {
  return `rgba(${color.r}, ${color.g}, ${color.b}, ${withOpacity ? color.a : 1})`;
};
