/**
 * Concatenates an indefinite number of strings
 * (classNames) into one
 * @returns {string}
 */
export const clsx = (...rest): string =>
  [...rest]
    // only keep strings
    .filter(s => typeof s === "string")
    .join(" ")
    .trim();

/*
 * Generates unique (length 9) hash
 */
export const uniqId = (): string =>
  "_" +
  Math.random()
    .toString(36)
    .substr(2, 9);

/**
 * Round num to the closest p places given.
 */
export const roundToNPlace = (num: number, p = 2): number =>
  +(Math.round(num * 100) / 100).toFixed(p);
