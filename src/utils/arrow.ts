import Konva from "konva";
import { RectConf } from "konva/types/Util";

type CircleConf = Konva.Vector2d & { radius: number };
type Vertex = {
  x1: number;
  y1: number;
  x2: number;
  y2: number;
};

/**
 * Given a (height, width, x, y) returns array with all
 * rect vertices.
 *
 * @param rect
 * @return vertices
 */
const getRectVertices = (rect: RectConf): Vertex[] => {
  return [
    {
      x1: rect.x,
      x2: rect.x + rect.width,
      y1: rect.y,
      y2: rect.y
    },
    {
      x1: rect.x,
      x2: rect.x,
      y1: rect.y,
      y2: rect.y + rect.height
    },
    {
      x1: rect.x + rect.width,
      x2: rect.x + rect.width,
      y1: rect.y,
      y2: rect.y + rect.height
    },
    {
      x1: rect.x,
      x2: rect.x + rect.width,
      y1: rect.y + rect.height,
      y2: rect.y + rect.height
    }
  ];
};

/**
 * Calculates the closest point on vertex from given point
 * @param vertex - vertex to get closest point on
 * @param point -  point from which we check for the closest point on vertex
 */
const getClosestPointOnVertex = (vertex: Vertex, point: Konva.Vector2d) => {
  // a,b are the points that construct the vertex
  const a = { x: vertex.x1, y: vertex.y1 };
  const b = { x: vertex.x2, y: vertex.y2 };

  const ap = [point.x - a.x, point.y - a.y];
  const ab = [b.x - a.x, b.y - a.y];

  const atb2 = ab[0] ** 2 + ab[1] ** 2;
  const atpDotAtb = ap[0] * ab[0] + ap[1] * ab[1];
  const leverRatio = atpDotAtb / atb2;

  let closestPoint: Konva.Vector2d;
  if (leverRatio < 0) {
    closestPoint = a;
  } else if (leverRatio > 1) {
    closestPoint = b;
  } else {
    closestPoint = { x: a.x + ab[0] * leverRatio, y: a.y + ab[1] * leverRatio };
  }

  const distance = Math.sqrt(
    Math.pow(point.x - closestPoint.x, 2) + Math.pow(point.y - closestPoint.y, 2)
  );

  return { point: closestPoint, distance, leverRatio } as const;
};

/**
 * Gets closest point on rect vertices from given point
 * @param rect
 * @param point
 */
export const getClosestPointToRect = (rect: RectConf, point: Konva.Vector2d): Konva.Vector2d => {
  const lines = getRectVertices(rect);
  const closestLinePoints = lines.map(line => getClosestPointOnVertex(line, point));

  const closest = closestLinePoints.reduce((closest, curr) => {
    if (!closest) return curr;

    if (curr.leverRatio < 1 && curr.leverRatio > 0) {
      if (closest.leverRatio < 1 && closest.leverRatio > 0) {
        return closest.distance < curr.distance ? closest : curr;
      } else {
        return curr;
      }
    } else {
      return closest.distance < curr.distance ? closest : curr;
    }
  });

  return closest.point;
};

/**
 *
 * Gets closest point on a circle from given point.
 * @param circle
 * @param point
 */
export const getClosestPointToCircle = (
  circle: CircleConf,
  point: Konva.Vector2d
): Konva.Vector2d => {
  const ab = [point.x - circle.x, point.y - circle.y];

  const angle = Math.atan2(-ab[1], ab[0]);

  return {
    x: circle.x - circle.radius * Math.cos(angle + Math.PI),
    y: circle.y + circle.radius * Math.sin(angle + Math.PI)
  } as const;
};
