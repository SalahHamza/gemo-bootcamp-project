import { createMuiTheme } from "@material-ui/core/styles";
import { Theme } from "@material-ui/core";
import purple from "@material-ui/core/colors/purple";
import yellow from "@material-ui/core/colors/yellow";

export default (type: "dark" | "light" | undefined = "dark"): Theme =>
  createMuiTheme({
    palette: {
      primary: { ...purple, main: purple[300] },
      secondary: yellow,
      type
    }
  });
