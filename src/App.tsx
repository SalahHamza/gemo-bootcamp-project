import React from "react";
import AppRoutes from "./routes";
import AppThemeProvider from "components/ThemeProvider";
// We add CssBaseline to normalize styles
// read more here:
// https://material-ui.com/components/css-baseline/#css-baseline
import CssBaseline from "@material-ui/core/CssBaseline";

const App: React.FC = () => {
  return (
    <React.Fragment>
      <CssBaseline />
      <AppThemeProvider>
        <AppRoutes />
      </AppThemeProvider>
    </React.Fragment>
  );
};

export default App;
