import React, { Suspense, lazy } from "react";
import { Switch, Route } from "react-router-dom";
import { appRoutes, EnumRouteItem } from "./appRoutes";
import Layout from "containers/Layout";

const NotFound = lazy(() => import(/* webpackChunkName: "NotFound" */ "containers/NotFound"));

const Routes: React.FC = () => {
  return (
    <Layout>
      <Suspense fallback={<div>loading...</div>}>
        <Switch>
          {appRoutes.map((route: EnumRouteItem) => (
            <Route key={route.path} path={route.path} exact>
              <route.Component />
            </Route>
          ))}
          <Route>
            <NotFound />
          </Route>
        </Switch>
      </Suspense>
    </Layout>
  );
};

export default Routes;
