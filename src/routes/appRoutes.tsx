import { lazy } from "react";

// Note: change this with loadable components
const Home = lazy(() => import(/* webpackChunkName: "Home" */ "containers/Home"));

const Report = lazy(() => import(/* webpackChunkName: "Report" */ "containers/Report"));

export interface EnumRouteItem {
  Component: React.ComponentType;
  path: string;
  exact: boolean;
}

export const appRoutes: EnumRouteItem[] = [
  {
    Component: Home,
    path: "/",
    exact: true
  },
  {
    Component: Report,
    path: "/report",
    exact: true
  }
];
