import React from "react";
import { useCanvasRootCTX } from "contexts/CanvasRootContext";

/**
 * gets the image element of the current
 * canvas image.
 */
export default function useGetCanvasImage(): HTMLImageElement | undefined {
  const [image, setImage] = React.useState<HTMLImageElement>();

  const { state: rootState } = useCanvasRootCTX();
  const stageItem = rootState.current ? rootState.images[rootState.current] : null;

  // it's okay to put this outside the useEffect
  // since it's a string.
  const imageSrc = stageItem?.src;

  React.useEffect(() => {
    if (!imageSrc) return;

    const image = new window.Image();
    image.src = imageSrc;

    const handleLoad = (): void => {
      setImage(image);
    };
    image.addEventListener("load", handleLoad);

    return (): void => {
      // we cleanup on "unmount"
      image.removeEventListener("load", handleLoad);
    };
  }, [imageSrc]);

  return image;
}
