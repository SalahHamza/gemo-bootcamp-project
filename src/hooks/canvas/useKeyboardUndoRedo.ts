import React from "react";
import { useCanvasRootCTX } from "contexts/CanvasRootContext";
import * as lo from "lodash";

/**
 * Adds keyboard support to canvas undo/redo.
 */
export default function useKeyboardUndoRedo(): void {
  const { dispatch: rootDispatch } = useCanvasRootCTX();

  React.useEffect(() => {
    const handleKeyboardUndoRedo = lo.throttle((event): void => {
      const evt = window.event ? window.event : event;
      // we detect cmd + shift + Z
      if (evt.metaKey || evt.ctrlKey) {
        evt.preventDefault();

        if (evt.shiftKey && evt.keyCode === 90) {
          rootDispatch({ type: "redo" });
          // we detect cmd + Z
        } else if (evt.keyCode === 90) {
          rootDispatch({ type: "undo" });
        }
      }
    }, 650);

    window.addEventListener("keydown", handleKeyboardUndoRedo);
    return (): void => {
      window.removeEventListener("keydown", handleKeyboardUndoRedo.cancel);
    };
  }, [rootDispatch]);
}
