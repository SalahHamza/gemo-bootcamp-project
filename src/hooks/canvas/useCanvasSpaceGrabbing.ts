import React from "react";
import { useCanvasConfCTX } from "contexts/CanvasConfContext";

/**
 * This hook takes care of the grabbing state
 * of the canvas component.
 */
export default function useCanvasSpaceGrabbing(): void {
  const { dispatch: confDispatch, state: confState } = useCanvasConfCTX();

  React.useEffect(() => {
    const handleKeydown = (event): void => {
      if (event.isComposing || event.keyCode === 229) {
        return;
      }
      // we set the grab state to true if it's not already
      // and if the pressed key is space
      if (event.code === "Space" && !confState.canGrab) {
        confDispatch({ type: "set_cursor_grabbing", payload: true });
      }
    };

    const handleKeyUp = (event): void => {
      if (event.isComposing || event.keyCode === 229) {
        return;
      }
      // we set the grab state to true if it's not already
      // and if the pressed key is space
      if (event.code === "Space" && confState.canGrab) {
        confDispatch({ type: "set_cursor_grabbing", payload: false });
      }
    };

    window.addEventListener("keydown", handleKeydown);
    window.addEventListener("keyup", handleKeyUp);

    return (): void => {
      // we cleanup the events on cycle end
      window.removeEventListener("keydown", handleKeydown);
      window.removeEventListener("keyup", handleKeydown);
    };
  }, [confDispatch, confState.canGrab]);
}
