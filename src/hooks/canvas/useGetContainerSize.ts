import React from "react";

type Size = {
  height: number;
  width: number;
};
type ContainerType = {
  ref: React.RefObject<HTMLDivElement>;
  size?: Size;
};

/**
 * Gets width/height of ref'd container.
 *
 *
 * Usage: pass ref to the container you want to get
 * width/height of.
 */
export default function useGetContainerSize(): ContainerType {
  const containerRef = React.useRef<HTMLDivElement>(null);
  const [size, setSize] = React.useState<Size>();

  React.useEffect(() => {
    const container = containerRef.current as HTMLDivElement;

    const handleSizeChange = (): void => {
      setSize({
        height: container.offsetHeight,
        width: container.offsetWidth
      });
    };

    // we set the initial size
    handleSizeChange();

    // then we listen for changes in the window size.
    window.addEventListener("resize", handleSizeChange);
    return (): void => {
      window.removeEventListener("resize", handleSizeChange);
    };
  }, []);

  return { size, ref: containerRef };
}
