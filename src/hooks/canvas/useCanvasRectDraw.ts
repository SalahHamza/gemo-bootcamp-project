import React from "react";
import { getRelativePointerPosition, isOutsideNode } from "utils/canvas";
import { RectConf } from "konva/types/Util";
import { useCanvasConfCTX } from "contexts/CanvasConfContext";
import Konva from "konva";
import { CANVAS_IMAGE_NAME, CANVAS_SNAP_THRESHOLD } from "constants/canvas";

type DrawedRect = RectConf & { valid: boolean; type: "rect" };

type CanvasRectDraw = {
  /**
   * triggers the rect drawing start.
   * @note Pass it to stage onMouseDown.
   */
  onRectDrawStart: (event) => void;
  /**
   * handles the rect drawing;
   * @note Pass it to stage onMouseMove.
   */
  onRectDrawing: (event) => void;
  /**
   * handles the rect drawing;
   * @note Pass it to stage onMouseUp.
   */
  onRectDrawEnd: (event) => void;
  /**
   * the temp drawed rect.
   */
  rect: DrawedRect | null;
};

/**
 * Creates a temp drawable rect
 */
const useCanvasRectDraw = (): CanvasRectDraw => {
  const [rect, setRect] = React.useState<DrawedRect | null>(null);
  const { state: confState } = useCanvasConfCTX();

  const canDraw = confState.cursor === "drawer:rect" && !confState.canGrab;

  const onRectDrawStart = React.useCallback(
    (event): void => {
      if (!canDraw) return;
      const stage = event.currentTarget.getStage() as Konva.Stage;
      const image = stage?.findOne(`.${CANVAS_IMAGE_NAME}`) as Konva.Image;

      if (isOutsideNode(image)) {
        // we give the user a visual hint
        stage.container().style.cursor = "not-allowed";
        // then abort
        return;
      }

      const pos = getRelativePointerPosition(stage);
      setRect({
        x: pos.x,
        y: pos.y,
        width: 0,
        height: 0,
        valid: true,
        type: "rect"
      });
    },
    [canDraw]
  );

  const onRectDrawing = React.useCallback(
    (event): void => {
      if (!canDraw || !rect) return;
      const stage = event.currentTarget.getStage() as Konva.Stage;
      const image = stage?.findOne(`.${CANVAS_IMAGE_NAME}`) as Konva.Image;

      const pos = getRelativePointerPosition(stage);
      const relPos = getRelativePointerPosition(image);

      let newWidth;
      // we check if we are at the left bound of the image
      if (Math.abs(relPos.x) <= CANVAS_SNAP_THRESHOLD) {
        newWidth = pos.x - rect.x - relPos.x;
        // we check if we are at the right bound of the image
      } else if (Math.abs(relPos.x - image.width()) <= CANVAS_SNAP_THRESHOLD) {
        newWidth = pos.x - rect.x - relPos.x + image.width();
      } else {
        newWidth = pos.x - rect.x;
      }

      let newHeight;
      // we check if we are at the top bound of the image
      if (Math.abs(relPos.y) <= CANVAS_SNAP_THRESHOLD) {
        newHeight = pos.y - rect.y - relPos.y;
        // we check if we are at the bottom bound of the image
      } else if (Math.abs(relPos.y - image.height()) <= CANVAS_SNAP_THRESHOLD) {
        newHeight = pos.y - rect.y - relPos.y + image.height();
      } else {
        newHeight = pos.y - rect.y;
      }

      // we finally check if the rectangle is still inside the
      // image frame.
      // Alternatively we can use isOutsideNode(), but it calls
      // getRelativePointerPosition() inside it, so there is no
      // need to recalculate the relative position again.
      const isValidRect =
        relPos.x + CANVAS_SNAP_THRESHOLD >= 0 &&
        relPos.y + CANVAS_SNAP_THRESHOLD >= 0 &&
        relPos.x - CANVAS_SNAP_THRESHOLD <= image.width() &&
        relPos.y - CANVAS_SNAP_THRESHOLD <= image.height();

      setRect({
        ...rect,
        width: newWidth,
        height: newHeight,
        valid: isValidRect
      });
    },
    [canDraw, rect]
  );

  const onRectDrawEnd = React.useCallback(
    (event): void => {
      if (!canDraw) return;
      const stage = event.currentTarget.getStage() as Konva.Stage;
      // we remove any visual hint in case it's there
      stage.container().style.cursor = "inherit";

      setRect(null);
    },
    [canDraw]
  );

  return {
    onRectDrawStart,
    onRectDrawing,
    onRectDrawEnd,
    rect
  };
};

export default useCanvasRectDraw;
