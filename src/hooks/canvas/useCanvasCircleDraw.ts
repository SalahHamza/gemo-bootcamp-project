import React from "react";
import { getRelativePointerPosition, isOutsideNode } from "utils/canvas";
import { useCanvasConfCTX } from "contexts/CanvasConfContext";
import Konva from "konva";
import { CANVAS_IMAGE_NAME, CANVAS_SNAP_THRESHOLD } from "constants/canvas";

type DrawedCircle = {
  valid: boolean;
  type: "circle";
  radius: number;
} & Konva.Vector2d;

type CanvasRectDraw = {
  /**
   * triggers the circle  drawing start.
   * @note Pass it to stage onMouseDown.
   */
  onCircleDrawStart: (event) => void;
  /**
   * handles the circle  drawing;
   * @note Pass it to stage onMouseMove.
   */
  onCircleDrawing: (event) => void;
  /**
   * handles the circle  drawing;
   * @note Pass it to stage onMouseUp.
   */
  onCircleDrawEnd: (event) => void;
  /**
   * the temp drawed circle .
   */
  circle: DrawedCircle | null;
};

/**
 * Creates a temp drawable circle
 */
const useCanvasCircleDraw = (): CanvasRectDraw => {
  const [circle, setCircle] = React.useState<DrawedCircle | null>(null);
  const { state: confState } = useCanvasConfCTX();

  const canDraw = confState.cursor === "drawer:circle" && !confState.canGrab;

  const onCircleDrawStart = React.useCallback(
    (event): void => {
      if (!canDraw) return;
      const stage = event.currentTarget.getStage() as Konva.Stage;
      const image = stage?.findOne(`.${CANVAS_IMAGE_NAME}`) as Konva.Image;

      if (image && isOutsideNode(image)) {
        // we give the user a visual hint
        stage.container().style.cursor = "not-allowed";
        // then abort
        return;
      }

      const pos = getRelativePointerPosition(stage);
      setCircle({
        x: pos.x,
        y: pos.y,
        radius: 0,
        valid: true,
        type: "circle"
      });
    },
    [canDraw]
  );

  const onCircleDrawing = React.useCallback(
    (event): void => {
      if (!canDraw || !circle) return;

      const stage = event.currentTarget.getStage() as Konva.Stage;
      const image = stage?.findOne(`.${CANVAS_IMAGE_NAME}`) as Konva.Image;

      const pos = getRelativePointerPosition(stage);

      const imageW = image.width();
      const imageH = image.height();

      const imageX = image.x() - image.offsetX();
      const imageY = image.y() - image.offsetY();

      let radius = Math.max(Math.abs(pos.x - circle.x), Math.abs(pos.y - circle.y));

      if (Math.abs(circle.x - radius - imageX) <= CANVAS_SNAP_THRESHOLD) {
        radius = imageX - circle.x;
      } else if (Math.abs(imageX + imageW - circle.x - radius) <= CANVAS_SNAP_THRESHOLD) {
        radius = imageX + imageW - circle.x;
      } else if (Math.abs(circle.y - radius - imageY) <= CANVAS_SNAP_THRESHOLD) {
        radius = imageY - circle.y;
      } else if (Math.abs(imageY + imageH - circle.y - radius) <= CANVAS_SNAP_THRESHOLD) {
        radius = imageY + imageH - circle.y;
      }

      // we finally check if the circle is still inside the
      // image frame.
      const isInsideImage =
        circle.x - circle.radius >= imageX &&
        circle.x + circle.radius <= imageX + imageW &&
        circle.y - circle.radius >= imageY &&
        circle.y + circle.radius <= imageY + imageH;

      setCircle({
        ...circle,
        radius: Math.abs(radius),
        valid: isInsideImage
      });
    },
    [canDraw, circle]
  );

  const onCircleDrawEnd = React.useCallback(
    (event): void => {
      if (!canDraw) return;
      const stage = event.currentTarget.getStage() as Konva.Stage;

      // we remove any visual hint in case it's there
      stage.container().style.cursor = "inherit";

      setCircle(null);
    },
    [canDraw]
  );

  return {
    onCircleDrawStart,
    onCircleDrawing,
    onCircleDrawEnd,
    circle
  };
};

export default useCanvasCircleDraw;
