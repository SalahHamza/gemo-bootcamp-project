import React from "react";
import useCanvasRectDraw from "./useCanvasRectDraw";
import useCanvasCircleDraw from "./useCanvasCircleDraw";
import { useCanvasConfCTX } from "contexts/CanvasConfContext";

const useCanvasShapeDraw = () => {
  const { rect, onRectDrawStart, onRectDrawing, onRectDrawEnd } = useCanvasRectDraw();
  const { circle, onCircleDrawStart, onCircleDrawing, onCircleDrawEnd } = useCanvasCircleDraw();
  const { state: confState } = useCanvasConfCTX();

  return React.useMemo(() => {
    switch (confState.cursor) {
      case "drawer:rect":
        return {
          shape: rect,
          onDrawStart: onRectDrawStart,
          onDrawing: onRectDrawing,
          onDrawEnd: onRectDrawEnd
        };
      case "drawer:circle":
        return {
          shape: circle,
          onDrawStart: onCircleDrawStart,
          onDrawing: onCircleDrawing,
          onDrawEnd: onCircleDrawEnd
        };
      default:
        return null;
    }
  }, [
    circle,
    confState.cursor,
    onCircleDrawEnd,
    onCircleDrawStart,
    onCircleDrawing,
    onRectDrawEnd,
    onRectDrawStart,
    onRectDrawing,
    rect
  ]);
};

export default useCanvasShapeDraw;
