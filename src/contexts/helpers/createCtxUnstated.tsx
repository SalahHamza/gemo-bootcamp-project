/* eslint-disable @typescript-eslint/explicit-function-return-type */
import React from "react";

/**
 * Creates context provider and its consumer hook.
 * This function requires a default value unlike createCtx.
 *
 * More details here:
 * https://github.com/typescript-cheatsheets/react-typescript-cheatsheet#context
 */
export default function createCtxUnstated<A>(defaultValue: A) {
  type UpdateType = React.Dispatch<React.SetStateAction<typeof defaultValue>>;
  const defaultUpdate: UpdateType = () => defaultValue;

  const ctx = React.createContext({
    state: defaultValue,
    update: defaultUpdate
  });

  function Provider(props: React.PropsWithChildren<{}>) {
    const [state, update] = React.useState(defaultValue);

    return <ctx.Provider value={{ state, update }} {...props} />;
  }

  function useCtx() {
    const c = React.useContext(ctx);
    if (!c) throw new Error("useCtx must be inside a Provider with a value");
    return c;
  }

  return [useCtx, Provider] as const;
}
