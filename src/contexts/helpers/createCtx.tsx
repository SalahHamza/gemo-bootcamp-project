/* eslint-disable @typescript-eslint/explicit-function-return-type */
import React from "react";

/**
 * Creates context provider and its consumer hook.
 * More details here:
 * https://github.com/typescript-cheatsheets/react-typescript-cheatsheet#context
 */
export default function createCtx<A>() {
  const ctx = React.createContext<A | undefined>(undefined);
  function useCtx() {
    const c = React.useContext(ctx);
    if (!c) throw new Error("useCtx must be inside a Provider with a value");
    return c;
  }
  return [useCtx, ctx.Provider] as const;
}
