/* eslint-disable @typescript-eslint/explicit-function-return-type */
import React from "react";

/**
 * utility to create a context with a reducer
 * srouce here:
 * https://gist.github.com/sw-yx/f18fe6dd4c43fddb3a4971e80114a052
 *
 * @todo: separate state/dispatch providers into 2
 * this way when the changes components using
 * useDispatch won't update as well.
 * More details: https://kentcdodds.com/blog/how-to-use-react-context-effectively
 */
export default function createReducerCtx<StateType, ActionType>(
  reducer: React.Reducer<StateType, ActionType>,
  initialState: StateType
) {
  const defaultDispatch: React.Dispatch<ActionType> = () => initialState; // we never actually use this

  const ctx = React.createContext({
    state: initialState,
    dispatch: defaultDispatch // just to mock out the dispatch type and make it not optioanl
  });

  function Provider(props: React.PropsWithChildren<{}>) {
    const [state, dispatch] = React.useReducer<React.Reducer<StateType, ActionType>>(
      reducer,
      initialState
    );

    return <ctx.Provider value={{ state, dispatch }} {...props} />;
  }

  function useCtx() {
    const c = React.useContext(ctx);
    if (!c) throw new Error("useCtx must be inside a Provider with a value");
    return c;
  }
  return [useCtx, Provider] as const;
}
