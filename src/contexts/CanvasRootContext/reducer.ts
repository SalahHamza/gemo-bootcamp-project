import { ShapeItem, ImageItem, ScreenItem } from "constants/canvas";
import Konva from "konva";
import * as helpers from "./helpers";

type CanvasItem = {
  id: string;
  src: string;
  shapes: ShapeItem[];
  selected: string | null;
  scale: number;
  stageX?: number;
  stageY?: number;
  history: {
    // we don't want to also store the history
    // within another version of itself.
    items: Omit<CanvasItem, "history">[];
    current: number;
  };
};

export type State = {
  images: {
    [key: string]: CanvasItem;
  };
  current: string | null;
  stage: Konva.Stage | null;
  screenshots: ScreenItem[];
  screenshotModal: {
    image: string | null;
    details?: string;
    open: boolean;
  };
};

export const initialState: State = {
  images: {},
  current: null,
  stage: null,
  // screenshot modal open/close status
  screenshots: [],
  screenshotModal: {
    open: false,
    image: null
  }
};

type Action =
  | { type: "add_shape"; payload: ShapeItem }
  | { type: "update_shape"; payload: ShapeItem }
  | {
      type: "update_annotation_position";
      payload: {
        pos: Konva.Vector2d;
        shapeId: string;
      };
    }
  | { type: "remove_shape"; payload: string }
  | { type: "toggle_shapes_visibility"; payload: boolean }
  | { type: "set_selected"; payload: string | null }
  | { type: "set_stage_node"; payload: Konva.Stage }
  | { type: "set_stage_scale"; payload: number }
  | { type: "set_stage_pos"; payload: Konva.Vector2d }
  | { type: "add_img"; payload: ImageItem }
  | { type: "delete_img"; payload: string }
  | { type: "set_current_img"; payload: string | null }
  | { type: "undo" }
  | { type: "redo" };

export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case "add_shape":
      return helpers.addShape(state, action.payload);
    case "update_shape":
      return helpers.updateShape(state, action.payload);
    case "update_annotation_position":
      return helpers.updateAnnotationPosition(state, action.payload);
    case "remove_shape":
      return helpers.removeShape(state, action.payload);
    case "toggle_shapes_visibility":
      return helpers.toggleShapesVisbility(state, action.payload);
    case "set_selected":
      return helpers.setSelectedShape(state, action.payload);
    case "set_stage_node":
      return helpers.setStageNode(state, action.payload);
    case "set_stage_scale":
      return helpers.setStageScale(state, action.payload);
    case "set_stage_pos":
      return helpers.setStagePos(state, action.payload);
    case "add_img":
      return helpers.addImage(state, action.payload);
    case "set_current_img":
      return helpers.setCurrentImage(state, action.payload);
    case "delete_img":
      return helpers.deleteImage(state, action.payload);
    case "undo":
      return helpers.undo(state);
    case "redo":
      return helpers.redo(state);
    default:
      throw new Error();
  }
}

export default reducer;
