import createReducerCtx from "contexts/helpers/createReducerCtx";
import reducer, { initialState } from "./reducer";
import createCtx from "contexts/helpers/createCtx";

const ctx = createReducerCtx(reducer, initialState);

// we only expose the hook/provider
export const [useCanvasRootCTX, CanvasRootProvider] = ctx;

/**
 * The canvas stage doesn't allow the value of the context
 * to reach its children, this context serves as a bridge
 * to the 'CanvasRootProvider'.
 * more details here:
 * https://github.com/konvajs/react-konva/issues/188
 */

const [useCtx, Provider] = createCtx();

export const useCanvasRootBridge = useCtx as typeof useCanvasRootCTX;
export const CanvasRootBridgeProvider = Provider;
