import { State } from "./reducer";
import {
  ShapeItem,
  ImageItem,
  CANVAS_DEFAULT_SCALE,
  CANVAS_MAX_SCALE,
  CANVAS_MIN_SCALE
} from "constants/canvas";
import Konva from "konva";
import * as lo from "lodash";

/**
 * add shape action reducer
 * @param state - root context state
 * @param shape - shape to add
 * @return updated state
 */
export const addShape = (state: State, shape: ShapeItem): State => {
  const imageId = state.current;
  if (!imageId) return state;

  let stageItem = state.images[imageId];
  stageItem = {
    ...stageItem,
    // the order here is important, as the last item
    // in the array is the one showing on top.
    shapes: stageItem.shapes.concat(shape),
    selected: shape.id
  };

  return historicize({
    ...state,
    images: {
      ...state.images,
      [imageId]: stageItem
    }
  });
};

/**
 * update shape action reducer
 * @param state - root context state
 * @param shape - shape to update
 * @return updated state
 */
export const updateShape = (state: State, shape: ShapeItem): State => {
  const imageId = state.current;
  if (!imageId) return state;

  let stageItem = state.images[imageId];
  stageItem = {
    ...stageItem,
    // we want to keep the item in the same index
    // since shapes z-index depends on their order on the array.
    shapes: stageItem.shapes.map(item => (item.id === shape.id ? shape : item)),
    selected: shape.id
  };
  return historicize({
    ...state,
    images: {
      ...state.images,
      [imageId]: stageItem
    }
  });
};

export const updateAnnotationPosition = (
  state: State,
  { shapeId, pos }: { shapeId: string; pos: Konva.Vector2d }
) => {
  const imageId = state.current;
  if (!imageId) return state;

  let stageItem = state.images[imageId];

  const shapesArr = stageItem.shapes.map(shape => {
    if (shape.id !== shapeId) return shape;
    return {
      ...shape,
      annotationPos: pos
    };
  });

  stageItem = {
    ...stageItem,
    // we want to keep the item in the same index
    // since shapes z-index depends on their order on the array.
    shapes: shapesArr
  };

  return historicize({
    ...state,
    images: {
      ...state.images,
      [imageId]: stageItem
    }
  });
};

/**
 * set selected shape action reducer (not historicize)
 * @param state - root context state
 * @param shapeId - shape id to select
 * @return updated state
 */
export const setSelectedShape = (state: State, shapeId: string | null): State => {
  const imageId = state.current;
  if (!imageId) return state;

  let stageItem = state.images[imageId];
  stageItem = {
    ...stageItem,
    // we want to keep the item in the same index
    // since shapes z-index depends on their order on the array.
    selected: shapeId
  };

  return {
    ...state,
    images: {
      ...state.images,
      [imageId]: stageItem
    }
  };
};

/**
 * remove shape action reducer
 * @param state - root context state
 * @param shapeId - shape id to delete
 * @return updated state
 */
export const removeShape = (state: State, shapeId: string): State => {
  const imageId = state.current;
  if (!imageId) return state;

  let stageItem = state.images[imageId];
  stageItem = {
    ...stageItem,
    // we want to keep the item in the same index
    // since shapes z-index depends on their order on the array.
    shapes: stageItem.shapes.filter(item => item.id !== shapeId)
  };
  return historicize({
    ...state,
    images: {
      ...state.images,
      [imageId]: stageItem
    }
  });
};

/**
 * toggle all current shapes' visbility action reducer
 * @param state - root context state
 * @return updated state
 */
export const toggleShapesVisbility = (state: State, payload: boolean): State => {
  const imageId = state.current;
  if (!imageId) return state;
  let stageItem = state.images[imageId];
  // we don't need to update if no shapes exist
  if (!stageItem.shapes.length) return state;

  stageItem = {
    ...stageItem,
    // we toggle all current shapes
    shapes: stageItem.shapes.map(shape => ({
      ...shape,
      show: payload
    }))
  };

  return historicize({
    ...state,
    images: {
      ...state.images,
      [imageId]: stageItem
    }
  });
};

/**
 * add image action reducer (not historicized)
 * @param state - root context state
 * @param image - image item to add
 * @return updated state
 */
export const addImage = (state: State, image: ImageItem): State => {
  return {
    ...state,
    images: {
      ...state.images,
      [image.id]: {
        id: image.id,
        src: image.src,
        shapes: [],
        selected: null,
        history: {
          items: [],
          current: -1
        },
        scale: CANVAS_DEFAULT_SCALE
      }
    },
    current: image.id
  };
};

/**
 * set current image action reducer (not historicized)
 * @param state - root context state
 * @param imageId - image id to show
 * @return updated state
 */
export const setCurrentImage = (state: State, imageId: string | null): State => {
  return {
    ...state,
    current: imageId,
    // we need to set the stage to null so that it's
    // reset later to an up-to-date version.
    stage: null
  };
};

/**
 * delete image action reducer (not historicize)
 * @param state - root context state
 * @param imgId - image id to delete
 * @return updated state
 */
export const deleteImage = (state: State, imageId: string): State => {
  if (!imageId || !state.images.hasOwnProperty(imageId)) return state;
  const { [imageId]: deletedImage, ...images } = state.images;

  return {
    ...state,
    images,
    current: state.current === imageId ? Object.keys(images)?.[0] || null : state.current,
    // we also set the stage node to null
    // so that it could be reset
    stage: null
  };
};

/**
 * Set stage node action reducer
 * @param state - root context state
 * @param stage - konva node stage
 * @return updated state
 *
 */
export const setStageNode = (state: State, stage: Konva.Stage): State => {
  return {
    ...state,
    stage
  };
};

/**
 * Set stage scale action reducer
 * @param state - root context state
 * @param scale - new scale
 * @return updated state
 *
 */
export const setStageScale = (state: State, scale: number): State => {
  const imageId = state.current;
  if (!imageId) return state;

  const stageItem = {
    ...state.images[imageId],
    scale: Math.min(Math.max(scale, CANVAS_MIN_SCALE), CANVAS_MAX_SCALE)
  };

  return {
    ...state,
    images: {
      ...state.images,
      [imageId]: stageItem
    }
  };
};

/**
 * Set stage position action reducer
 * @param state - root context state
 * @param pos - new stageX/stageY
 * @return updated state
 *
 */
export const setStagePos = (state: State, pos: Konva.Vector2d): State => {
  const imageId = state.current;
  if (!imageId) return state;

  const stageItem = {
    ...state.images[imageId],
    stageX: pos.x,
    stageY: pos.y
  };

  return {
    ...state,
    images: {
      ...state.images,
      [imageId]: stageItem
    }
  };
};

/**
 * Undo action reducer
 * @param state - root context state
 * @return updated state
 */
export const undo = (state: State): State => {
  const imageId = state.current;
  if (!imageId) return state;

  let stageItem = state.images[imageId];
  // no need to undo if there is no prior state to go back to
  if (stageItem.history.current <= 0) return state;

  const historyStep = stageItem.history.current - 1;

  stageItem = {
    ...stageItem,
    history: {
      ...stageItem.history,
      current: historyStep
    },
    // we set the past state as the current
    ...lo.cloneDeep(stageItem.history.items[historyStep])
  };

  return {
    ...state,
    stage: historyStep <= 0 ? null : state.stage,
    images: {
      ...state.images,
      [imageId]: stageItem
    }
  };
};

/**
 * Redo action reducer
 * @param state - root context state
 * @return updated state
 */
export const redo = (state: State): State => {
  const imageId = state.current;
  if (!imageId) return state;

  let stageItem = state.images[imageId];

  if (stageItem.history.current === stageItem.history.items.length) return state;

  const historyStep = stageItem.history.current + 1;

  stageItem = {
    ...stageItem,
    history: {
      ...stageItem.history,
      current: historyStep
    },
    // we set the future state as the current
    ...lo.cloneDeep(stageItem.history.items[historyStep])
  };

  return {
    ...state,
    images: {
      ...state.images,
      [imageId]: stageItem
    }
  };
};

/**
 * Syncs state changes to the undo/redo history.
 *
 * @param newState - root context state
 * @return updated state
 */
function historicize(newState: State): State {
  const imageId = newState.current;
  if (!imageId) return newState;

  let stageItem = newState.images[imageId];
  const { history, ...toStore } = stageItem;

  stageItem = {
    ...stageItem,
    history: {
      items: history.items.slice(0, history.current + 1).concat(lo.cloneDeep(toStore)),
      current: history.current + 1
    }
  };

  return {
    ...newState,
    images: {
      ...newState.images,
      [imageId]: stageItem
    }
  };
}
