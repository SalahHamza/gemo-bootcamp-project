import createReducerCtx from "contexts/helpers/createReducerCtx";
import createCtx from "contexts/helpers/createCtx";
import reducer, { initialState } from "./reducer";

const ctx = createReducerCtx(reducer, initialState);

// we only expose the hook/provider
export const [useCanvasConfCTX, CanvasConfProvider] = ctx;

/**
 * The canvas stage doesn't allow the value of the context
 * to reach its children, this context serves as a bridge
 * to the 'CanvasConfProvider'.
 * more details here:
 * https://github.com/konvajs/react-konva/issues/188
 */

const [useCtx, Provider] = createCtx();
export const useCanvasConfBridge = useCtx as typeof useCanvasConfCTX;
export const CanvasConfBridgeProvider = Provider;
