import { CanvasToolItemType } from "constants/canvas";

type State = {
  cursor: CanvasToolItemType;
  canGrab: boolean;
};

export const initialState: State = {
  cursor: "pointer",
  // the reason this is not another cursor type
  // is that we want to allow the user to grab
  // during both "drawer" and "pointer" cursors
  // NOTE:
  // - grabbing allows the user to grab the canvas
  // and move around.
  // - we can also just detect the grabbing of the
  // stage node and change the cursor, but we need
  // this because:
  //    1. it's the declarative way,
  //    2. we need this to prevent other work (shape move, drawing).
  // inspired from FIGMA. (space + move)
  canGrab: false
};

type Action =
  | { type: "set_cursor"; payload: CanvasToolItemType }
  | { type: "set_cursor_grabbing"; payload: boolean };

export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case "set_cursor":
      return { ...state, cursor: action.payload };
    case "set_cursor_grabbing":
      return { ...state, canGrab: action.payload };
    default:
      throw new Error();
  }
}

export default reducer;
