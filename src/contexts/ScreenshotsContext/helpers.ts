import { State } from "./reducer";
import { ScreenItem } from "constants/canvas";

/**
 *
 * @param state - root context state
 * @param item - screenshot item to add
 * @return updated state
 */
export const addScreenshot = (state: State, item: ScreenItem): State => {
  return {
    ...state,
    items: state.items.concat(item),
    modal: null,
    draft: null
  };
};

/**
 *
 * @param state - root context state
 * @param itemId - screenshot item id to update
 * @return updated state
 */
export const updateScreenshot = (state: State, item: ScreenItem): State => {
  const newItems = state.items.slice();
  const itemIndex = state.items.findIndex(shot => shot.id === item.id);
  newItems.splice(itemIndex, 1, item);

  return {
    ...state,
    items: newItems
  };
};
/**
 *
 * @param state - root context state
 * @param image - screenshot item to re
 * @return updated state
 */
export const removeScreenshot = (state: State, itemId: string): State => {
  return {
    ...state,
    items: state.items.filter(item => item.id !== itemId),
    modal: null,
    draft: null
  };
};

/**
 * opens screenshot add modal
 * @param state - root context state
 * @param payload - image data url or src
 * @return updated state
 */
export const openScreenshotAddModal = (state: State, payload: string): State => {
  return {
    ...state,
    modal: "add",
    draft: {
      src: payload
    }
  };
};

/**
 * opens screenshot edit modal
 * @param state - root context state
 * @param payload - screen shot id to open modal
 * @return updated state
 */
export const openScreenshotEditModal = (state: State, payload: string): State => {
  return {
    ...state,
    modal: "edit",
    current: payload
  };
};

/**
 * closes screen shot modal (not historicized)
 * @param state - root context state
 * @param payload - image data url or src
 * @return updated state
 */
export const closeScreenshotModal = (state: State): State => {
  return {
    ...state,
    modal: null,
    draft: null
  };
};
/**
 * updates draft comment
 * @param state - root context state
 * @param payload - image data url or src
 * @return updated state
 */
export const updateDraft = (state: State, payload: string): State => {
  if (!state.draft) return state;
  return {
    ...state,
    draft: {
      ...state.draft,
      comment: payload
    }
  };
};
