import createReducerCtx from "contexts/helpers/createReducerCtx";
import reducer, { initialState } from "./reducer";

const ctx = createReducerCtx(reducer, initialState);

// we only expose the hook/provider
export const [useScreenshotsCTX, ScreenshotsProvider] = ctx;
