import { ScreenItem } from "constants/canvas";
import * as helpers from "./helpers";

export type State = {
  items: ScreenItem[];
  current: string | null;
  modal: "add" | "edit" | null;
  draft: null | {
    src: string;
    comment?: string;
    id?: string;
  };
};

export const initialState: State = {
  items: [],
  modal: null,
  current: null,
  draft: null
};

type Action =
  | { type: "add_screenshot"; payload: ScreenItem }
  | { type: "update_screenshot"; payload: ScreenItem }
  | { type: "remove_screenshot"; payload: string }
  | { type: "open_add_modal"; payload: string }
  | { type: "open_edit_modal"; payload: string }
  | { type: "close_modal" }
  | { type: "update_draft"; payload: string };

export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case "add_screenshot":
      return helpers.addScreenshot(state, action.payload);
    case "update_screenshot":
      return helpers.updateScreenshot(state, action.payload);
    case "remove_screenshot":
      return helpers.removeScreenshot(state, action.payload);
    case "open_add_modal":
      return helpers.openScreenshotAddModal(state, action.payload);
    case "open_edit_modal":
      return helpers.openScreenshotEditModal(state, action.payload);
    case "close_modal":
      return helpers.closeScreenshotModal(state);
    case "update_draft":
      return helpers.updateDraft(state, action.payload);
    default:
      throw new Error();
  }
}

export default reducer;
