import createReducerCtx from "contexts/helpers/createReducerCtx";
import { initialState, reducer } from "contexts/ReportRootContext/reducer";

export const [useReportContext, ReportProvider] = createReducerCtx(reducer, initialState);
