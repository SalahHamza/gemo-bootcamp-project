import { SEXES } from "components/DocCard";
import dayjs from "dayjs";

export type DocData = {
  firstName: string;
  lastName: string;
  sex: "male" | "female";
  address: string;
  phone: string;
  date: string;
};

type ValueOf<T> = T[keyof T];

const docValues = {
  firstName: "",
  lastName: "",
  sex: SEXES[0].value.toString() as "male",
  address: "",
  phone: "",
  date: dayjs(new Date()).format("MM/DD/YYYY")
};

export const initialState = {
  editMode: true,
  // we initialize the values state
  values: docValues
};

type ReportState = typeof initialState;

type Action =
  | { type: "toggle_edit" }
  | { type: "set_values"; payload: typeof docValues }
  | { type: "set_value"; payload: { key: keyof DocData; value: ValueOf<DocData> } };

export function reducer(state: ReportState, action: Action): ReportState {
  switch (action.type) {
    case "toggle_edit":
      return {
        ...state,
        editMode: !state.editMode
      };
    case "set_values":
      return {
        ...state,
        values: action.payload
      };
    case "set_value":
      return {
        ...state,
        values: {
          ...state.values,
          [action.payload.key]: action.payload.value
        }
      };
    default:
      throw new Error();
  }
}

export default reducer;
